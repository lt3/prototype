package org.d3lt.api.verbalization;

import java.util.List;

public interface DomainFragment {

	Object getRoot();

	String getID(Object node);
	List<String> getModifiers(Object node);
	String getType(Object node);
	
	int    getTripleTypeCount(Object node);
	Object getTripleType(Object node, int tripleTypeIndex);
	
	int    getObjectCount(Object subject, Object tripleType);
	Object getObject(Object subject, Object tripleType, int objectIndex);
}
