package org.d3lt.api.verbalization;

public interface Verbalizer {
 String createAST(DomainFragment fragment, TaskContext task);
}
