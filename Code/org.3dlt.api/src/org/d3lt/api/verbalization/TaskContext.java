package org.d3lt.api.verbalization;

import java.util.List;

public interface TaskContext {
 String getTaskID();
 
 List<String> getModifiers();
 
}
