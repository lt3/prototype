package org.d3lt.test;

import static org.junit.Assert.*;

import org.d3lt.api.verbalization.Verbalizer;
import org.d3lt.gfverbalization.DefaultVerbalizer;
import org.junit.Before;
import org.junit.Test;

public class ASTTest {

	Verbalizer verbalizer = null;
	
	@Before
	public void init()
	{
		verbalizer = new DefaultVerbalizer();
	}
	
	@Test
	public void testStubs() {
		TestDomainFragment domain = new TestDomainFragment("intake",  "require",  new String[] {"applicationForm", "taxStatement"},  
				"Activity",  "Document", "available");
		
		assertEquals(1, domain.getTripleTypeCount("intake"));
		assertEquals(2, domain.getObjectCount("intake", "create"));
	}
	
	@Test
	public void testASTGeneration() {
		TestTaskContext task = new TestTaskContext("query");
		TestDomainFragment domain = new TestDomainFragment("intake",  "create",  new String[] {"applicationForm", "taxStatement"},  
				"Activity",  "Document", "available");
		
		assertEquals("(query (require Activity Document intake (AndIndividual applicationForm taxStatement)))", verbalizer.createAST(domain, task));

	}
}
