package org.d3lt.test;

import java.util.Collections;
import java.util.List;

import org.d3lt.api.verbalization.TaskContext;

public class TestTaskContext implements TaskContext {

	String taskID;
	
	TestTaskContext(String taskID)
	{
		this.taskID = taskID;
	}
	
	@Override
	public String getTaskID() {
	
		return taskID;
	}

	@Override
	public List<String> getModifiers() {
		return Collections.EMPTY_LIST;
	}

}
