package org.d3lt.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.d3lt.api.verbalization.DomainFragment;

/**
 * Simple Impl, one triple, no modifiers...
 * 
 *
 */
public class TestDomainFragment implements DomainFragment {
	
	public TestDomainFragment() {
		// TODO Auto-generated constructor stub
	}
	
	String subject, object[], tripleType, subjectType, objectType, objectModifier;
	
	List<String> objectModifiers = null;
	
	public TestDomainFragment(String subject, String tripleType, String[] object,
			 String subjectType, String objectType, String objectModifier) {
		super();
		this.subject = subject;
		this.object = object;
		this.tripleType = tripleType;
		this.subjectType = subjectType;
		this.objectType = objectType;
		this.objectModifier = objectModifier;
		
		objectModifiers = new ArrayList<String>();
		objectModifiers.add(objectModifier);
	}

	@Override
	public Object getRoot() {
		// TODO Auto-generated method stub
		return subject;
	}

	@Override
	public String getID(Object node) {
		// TODO Auto-generated method stub
		return node.toString();
	}

	@Override
	public List<String> getModifiers(Object node) {
		// TODO Auto-generated method stub
		return !node.equals(subject) ? objectModifiers : Collections.EMPTY_LIST;
	}

	@Override
	public String getType(Object node) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	@Override
	public int getTripleTypeCount(Object node) {
		// TODO Auto-generated method stub
		return subject.equals(node) ? 1 : 0;
	}

	@Override
	public Object getTripleType(Object node, int tripleTypeIndex) {
		return subject.equals(node) ? tripleType : null;
	}

	
	@Override
	public int getObjectCount(Object node, Object tripleType) {
		// TODO Auto-generated method stub
		return node.equals(subject) && tripleType.equals(this.tripleType) ? object.length : 0;
	}

	public Object getObject(Object subject, Object tripleType, int objectIndex) {
		return subject.equals(this.subject) && tripleType.equals(tripleType)? object[objectIndex] : null;
	}

}
