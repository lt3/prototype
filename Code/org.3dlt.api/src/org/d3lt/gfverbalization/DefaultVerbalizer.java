package org.d3lt.gfverbalization;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.d3lt.api.verbalization.DomainFragment;
import org.d3lt.api.verbalization.TaskContext;
import org.d3lt.api.verbalization.Verbalizer;

public class DefaultVerbalizer implements Verbalizer {

	@Override
	public String createAST(DomainFragment fragment, TaskContext task) {
		StringWriter w = new StringWriter();

		try {
			writeTaskAST(fragment, task, w);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return w.toString();
	}

	void writeTaskAST(DomainFragment fragment, TaskContext context, Writer w)
			throws IOException {
		w.write("(");
		w.write(context.getTaskID());
		w.write(" ");
		
		writeDomainAST(fragment, fragment.getRoot(), w);
		
		//TODO insert task additional modifiers here...
		
		w.write(")");
	}

	void writeDomainAST(DomainFragment fragment, Object node, Writer w)
			throws IOException {
		
		List<Object> objectsWithChildren = new ArrayList<Object>();
		
		// write node, inc type
		// if child count > 0
		int ttc = fragment.getTripleTypeCount(node);

		// open ANDSTATEMENT
		w.write(ttc > 1? "(AndStatement ": "");
		
		
		// loop trough tt's subjects
		for(int i=0; i<ttc; i++)
			
		{
			Object tt = fragment.getTripleType(node, i);
			w.write("(");
			
			// print tt
			w.write(tt.toString());
			w.write(" ");
			
			// print subject
			writeDomainNodeAST(fragment, node, w);
			w.write(" ");
			
			
			// loop through objects
			int objectCount = fragment.getObjectCount(node, tt);
			
			
			w.write (objectCount > 1 ? "(AndIndividual " : "");
			
			for(int oi=0; oi < objectCount; oi++)
			{
				// print object oi
				Object object = fragment.getObject(node, tt, oi);
				writeDomainNodeAST(fragment, object, w);
				w.write(oi < objectCount-1 ? " ":"");
				// if object has children add it to the objects with children list
			}
			w.write (objectCount > 1 ? ")" : "");
			
			w.write (")");
			
		}
		
		
		w.write(ttc > 1? ")": "");
		
		
		// recurse over children
			
		
	}
	void writeDomainNodeAST(DomainFragment fragment, Object node, Writer w)
			throws IOException {
		
		List<String> modifiers = fragment.getModifiers(node);
		
		if(modifiers.size() > 0)
		{
			w.write("(modify ");
			
			for(String t: modifiers)
			{
				w.write(t+" ");
			}
		}
		
		w.write(fragment.getID(node));
		
		if(modifiers.size() > 0)
		{
			w.write(")");
		}
		
	}
	
}
