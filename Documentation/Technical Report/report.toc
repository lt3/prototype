\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}A 3D API for Verbalization}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Principles for the API}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}AST Generation}{3}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Assumptions}{3}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Mechanism}{3}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Examples}{4}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Java API}{4}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Domain Fragments}{4}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Task Context}{4}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Task}{4}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Next Steps}{4}{section.2.4}
\contentsline {chapter}{\numberline {3}A Pluggable API for Processing Lexical Resources}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}An API for Lexical Services}{6}{chapter.4}
\contentsline {section}{\numberline {4.1}NLP Resources}{6}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Conjugator}{6}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Lemmatizer}{6}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Parser}{6}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}POSTagger}{6}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}SentenceSplitter}{6}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Tokenizer}{7}{subsection.4.1.6}
\contentsline {section}{\numberline {4.2}Terminology Resources}{7}{section.4.2}
\contentsline {section}{\numberline {4.3}Multilingual Resources}{7}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}LanguageDetection}{7}{subsection.4.3.1}
