"""
Quick & dirty hack for collecting all relevant grammar files given a domain, task and language(s).
"""

import pystache
import codecs
import os
import argparse
import re
from shutil import rmtree


source_path = '../gf/'
target_path = '../target/'
built_in    = ['Paradigms','Syntax','Extra','Prelude','Constructors'] + ['Core','Basic']


def run():
    
    p = argparse.ArgumentParser(description='build_grammar')
    p.add_argument('-d','--domain', action='append', dest='domains', default=[],
                    help='Domain name (same as GF module name, e.g. OceanWildlife)')
    p.add_argument('-t','--task', action='append', dest='tasks', default=[],
                    help='Task name (same as GF module name, e.g. QA)')
    p.add_argument('-e','--extra', action='append', dest='extras', default=[],
                    help='Additional module to be loaded (e.g. Numeral, Date)')
    p.add_argument('-l','--language', action='append', dest='languages', default=[],
                    help='Language (e.g. Eng, Ger, Dut)')
    p.add_argument('-name', action='store', dest='name', default='App')
    p.add_argument('-startcat', action='store', dest='startcat', default='Utterance_Utt')
    p.add_argument('--lite', action='store_true')
    p.add_argument('--alltenses', action='store_true')

    args = p.parse_args()

    if args.alltenses: path = 'alltenses'
    else:              path = 'present' 

    build_grammar(args.name,args.startcat,args.extras,args.extras,args.domains,args.tasks,args.languages,path,args.lite)


def build_grammar(name,startcat,extension,extras,domains,tasks,languages,path,lite):

    app_dir = source_path+'application'
    if not os.path.exists(app_dir): os.makedirs(app_dir)
    # create empty target directory
    target_dir = target_path + name + '/'
    if os.path.exists(target_dir): 
         print 'WARNING Overwriting ' + target_dir
         rmtree(target_dir)
    os.makedirs(target_dir)
    # collect all relevant files
    files = []
    _addFiles(files,'Core',languages)
    _addFiles(files,'Basic',languages)
    _addFiles(files,'Clauses',languages)
    for f in extras+domains+tasks:
        _addFiles(files,f,languages)
    # ...including dependencies
    deps = []
    for f in files:
        deps += extract_dependencies(_mkFile(f),languages)
    deps = list(set(deps))
    print 'Found the following dependencies: ' + str(map(_mkFile,deps))
    for d in deps:
        if d not in files: files.append(d)
    # build context for rendering
    context = { 'name': name, 'startcat' : startcat, 'ext' : extension, 'path' : path }
    if lite: context['lite'] = 'true'
    if path == 'alltenses': context['alltenses'] = True
    _addCommaSeparatedList(context,'extras',extras)
    _addCommaSeparatedList(context,'domains',domains)
    _addCommaSeparatedList(context,'tasks',tasks)
    # render all files into target directory
    print 'Rendering files:'
    for f in list(set(files)):
        print '   ' + f[1]
        render_grammar_file(_mkFile(f),context,target_dir+f[1])
    # build application grammar
    abstract = _find('Application_abstract.mustache')
    concrete = _find('Application_concrete.mustache')
    print '   Application grammar:'
    if abstract is not None:
       print '   --> ' + target_dir+name+'.gf'
       render_grammar_file(_mkFile(abstract),context,target_dir+name+'.gf')
    if concrete is not None:
       for l in languages:
           context['l'] = l
           _addCommaSeparatedList(context,'domains',domains,l)
           _addCommaSeparatedList(context,'tasks',tasks,l)
           print '   --> ' + target_dir+name+l+'.gf'
           render_grammar_file(_mkFile(concrete),context,target_dir+name+l+'.gf')

def _addFiles(files,prefix,languages):
    abstract   = _find(prefix+'.gf')
    incomplete = _find(prefix+'I.gf')
    if abstract     is not None: files.append(abstract)
    if incomplete   is not None: files.append(incomplete)
    for l in languages:
        concrete = _find(prefix+l+'.gf')
        if concrete is not None: files.append(concrete)

def _addCommaSeparatedList(context,key,ls,lang=''):
    value = ''
    for l in ls: value += l+lang+', '
    context[key] = value[:-2]

def extract_dependencies(filename,languages):
    candidates = []
    deps       = []
    for line in open(filename,'r').readlines():
        ma = re.match('abstract .* = (.*) \*\* .*',line)
        mc = re.match('(concrete .* = )?.*open (.*) in .*',line)
        if   ma and ma.group(1): candidates += ma.group(1).split(','); break
        elif mc and mc.group(2): candidates += mc.group(2).split(','); break
    for c in filter(_not_built_in,candidates):
        _addFiles(deps,c.strip(),languages)
    return deps


def render_grammar_file(source_file,context,target_file):
    
    source = codecs.open(source_file,'r','utf-8').read()
    target = codecs.open(target_file,'w','utf-8')

    target.write(pystache.render(source,context))
    target.close()


def _find(target,silent=False):
    for (dirname,subdirs,files) in os.walk(source_path):
        if target in files: 
           return (dirname,target)
    if not silent: print 'WARNING Cannot find file ' + target
    return None

def _mkFile(f):
    return f[0]+'/'+f[1]

def _not_built_in(s):
    for b in built_in:
        if s.strip().startswith(b): return False
    return True


if __name__ == "__main__":
    run()
