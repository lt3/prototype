--# -path=.:prelude:{{path}}

concrete ClausesEng of Clauses = CoreEng ** ClausesI with 

   (Basic  = BasicEng),
   (Syntax = SyntaxEng) ** {

 {{#alltenses}}
 oper 

     mkIWouldLike = overload {
     mkIWouldLike : NP -> S = \ np -> mkS conditionalTense (mkCl i_NP (mkVP like_V2 i));
     mkIWouldLike : VP -> S = \ vp -> mkS conditionalTense (mkCl i_NP (mkVP like_VV p.vp));
     };

     like_V2 : V2 = mkV2 (mkV "like");
     like_VV : VV = mkVV (mkV "like");
 {{/alltenses}}

}
