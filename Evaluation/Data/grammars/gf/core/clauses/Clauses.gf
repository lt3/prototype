
abstract Clauses = Core ** {


 cat
  
    WhPron;
    WhAdv;

    Modality;

    Clause_Cl;

    Sentence_S;
    Question_QS;
    Imperative_Imp;

    Utterance_Utt;


 fun 


    clause : Statement -> Clause_Cl;

    say     : Sentence_S     -> Utterance_Utt;
    ask     : Question_QS    -> Utterance_Utt;
    prompt  : Imperative_Imp -> Utterance_Utt;


    ---- Coordination ---- 
 
    AndSentence, OrSentence : Sentence_S -> Sentence_S -> Sentence_S;


    ---- Declarative sentences ----

    present_pos : Clause_Cl -> Sentence_S;
    present_neg : Clause_Cl -> Sentence_S;
    {{#alltenses}}
    past_pos    : Clause_Cl -> Sentence_S;
    past_neg    : Clause_Cl -> Sentence_S;
    future_pos  : Clause_Cl -> Sentence_S;
    future_neg  : Clause_Cl -> Sentence_S;
    {{/alltenses}}

    ---- Modals ----

    MCan, MKnow, MMust, MWant : Modality;
    mod_clause : Modality -> Statement -> Clause_Cl;

    ---- Imperative ----

    imperative : Statement -> Imperative_Imp;

    ---- Questions ----

    YesNo : Clause_Cl -> Question_QS;

    queryAdv : WhAdv  -> Statement -> Question_QS;
    query    : WhPron -> Statement -> Question_QS; 

    ---- wh-forms ----
    
    Who  : WhPron;
    What : WhPron;
    Where, When, How, Why : WhAdv;

    WhichSg, WhichPl, HowMany : Class -> WhPron;

}
