--# -path=.:prelude:{{path}}

incomplete concrete ClausesI of Clauses = CoreI ** open Basic, Syntax in {


 lincat

    Modality = VV;

    Clause_Cl      = Cl;
    Sentence_S     = S;
    Question_QS    = QS;
    Imperative_Imp = Imp;
    Utterance_Utt  = Utt;

    WhPron = IP;
    WhAdv  = IAdv; 


 lin 


    clause p = mkClause p;

    say     s = mkUtt s;
    ask     q = mkUtt q;
    prompt  i = mkUtt i;


    ---- Coordination ----

    AndSentence  s1 s2 = mkS and_Conj s1 s2;
    OrSentence   s1 s2 = mkS or_Conj  s1 s2;


    ---- Declarative sentences ----

    present_pos c = mkS presentTense positivePol c;
    present_neg c = mkS presentTense negativePol c;
    {{#alltenses}}
    past_pos    c = mkS pastTense    positivePol c;
    past_neg    c = mkS pastTense    negativePol c;
    future_pos  c = mkS futureTense  positivePol c;
    future_neg  c = mkS futureTense  negativePol c;
    {{/alltenses}}

    ---- Modals ----

    MWant = want_VV;
    MCan  = can_VV;
    MKnow = can8know_VV;
    MMust = must_VV;

    mod_clause m p = mkCl p.np m p.vp;

    ---- Imperative ----

    imperative p = mkImp p.vp;

    ---- Questions ---- 

    queryAdv a p = mkQS (mkQCl a (mkClause p));
    query   wh p = variants { mkQS (mkQCl wh p.vp);
                              mkQS (mkQCl wh (mkClSlash p.np p.vpSlash)) };

    YesNo p = mkQS p;

    ---- wh-forms ----

    Who   = whoSg_IP;
    What  = whatSg_IP;
    Where = where_IAdv;
    When  = when_IAdv;
    How   = how_IAdv;
    Why   = why_IAdv;

    WhichSg c = mkIP which_IDet c.cn; 
    WhichPl c = mkIP whichPl_IDet c.cn;
    HowMany c = mkIP how8many_IDet c.cn;

}
