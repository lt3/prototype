
abstract Core = {


 cat
  
    Class;
    Datatype;

    Individual {{^lite}}Class{{/lite}};
    [Individual {{^lite}}Class{{/lite}}] {2};

    Literal{{^lite}} Datatype{{/lite}};

    Statement;


 fun 


    ---- Determiners ----

    The, Some, All, Most, Generic, 
    This, That, These, Those : {{^lite}}(c : {{/lite}}Class{{^lite}}){{/lite}} -> Individual {{^lite}}c{{/lite}};


    ---- Anaphora ----

    He, She, It, They  : {{^lite}}(c : Class) ->{{/lite}} Individual {{^lite}}c{{/lite}}; 
    Poss_He, Poss_She, 
    Poss_It, Poss_They : {{^lite}}(c : {{/lite}}Class{{^lite}}){{/lite}} -> Individual {{^lite}}c{{/lite}};


    ---- Coordination ----

    AndIndividual, OrIndividual : {{^lite}}(c : Class) ->{{/lite}} [Individual{{^lite}} c{{/lite}}] -> Individual{{^lite}} c{{/lite}}; 


    ---- Modification ----

    modify_SC : Class -> Statement -> Class;
    modify_SS : Class -> Statement -> Statement;
    modify_CC : Class -> Class     -> Class;


    ---- Semantically light expressions ----

    toBe, toHave : {{^lite}}(c1,c2 : Class) ->{{/lite}} Individual{{^lite}} c1{{/lite}} -> Individual{{^lite}} c2{{/lite}} -> Statement;

    -- prepostions: with, ...


    ---- Placeholder after NER ----

    NamedIndividual : {{^lite}}(c : Class) ->{{/lite}} Individual{{^lite}} c{{/lite}};


    ---- OWL stuff to worry about later 

    owl_Thing : Class;

    ---- Booleans ----

    xsd_boolean : Datatype;
    xsd_true    : Literal{{^lite}} xsd_boolean{{/lite}};
    xsd_false   : Literal{{^lite}} xsd_boolean{{/lite}};
    
    ---- Numerals ----

    xsd_integer            : Datatype;
    xsd_positiveInteger    : Datatype;
    xsd_nonNegativeInteger : Datatype;
    xsd_double             : Datatype;
    xsd_float              : Datatype;
    xsd_decimal            : Datatype;

    ---- Strings ----

    xsd_string  : Datatype;

    ---- Dates and times ----

    xsd_date       : Datatype;
    xsd_dateTime   : Datatype;
    xsd_gYear      : Datatype;
    xsd_gYearMonth : Datatype;
    
    ---- URIs ----

    xsd_anyURI : Datatype;

}
