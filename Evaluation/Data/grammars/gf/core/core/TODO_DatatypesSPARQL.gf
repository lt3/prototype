--# -path=.:prelude:{{path}}

concrete Core_OWLSPARQL of Core_OWL = Core_SPARQL ** open SPARQL in {

    lincat 
        Datatype = Str;
        Literal  = Ident;

    lin 
        -- owl_Thing = "<http://www.w3.org/2002/07/owl#Thing>";

        xsd_true  = mkIdent "true"  emptyQuery;
        xsd_false = mkIdent "false" emptyQuery;
}
