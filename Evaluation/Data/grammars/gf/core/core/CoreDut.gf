--# -path=.:prelude:{{path}}

concrete CoreDut of Core = CoreI with 

   (Basic  = BasicDut),
   (Syntax = SyntaxDut), 
   (Extra  = ExtraDut) ** open SyntaxDut, ParadigmsDut in {

   lin NamedIndividual{{^lite}} _{{/lite}} = mkNP (mkPN "XXX");

   oper
     XXX_De  = mkN "XXX" "XXX" de;
     XXX_Het = mkN "XXX" "XXX" het;

}
