--# -path=.:prelude:{{path}}

concrete CoreSPARQL of Core = open SPARQL in {


 lincat

    Class       = Str;

    Individual  = Var -> Ident;
 --[Individual] = ...

    Proposition = Query;
 

 lin   

    ---- Determiners ----

    The     c v = mkRDFtypeIdent (newVar v) c;
    Some    c v = mkRDFtypeIdent (newVar v) c;
    Most    c v = mkRDFtypeIdent (newVar v) c;
    All     c v = mkRDFtypeIdent (newVar v) c;

    Generic c v = mkRDFtypeIdent (newVar v) c;

    This    c v = mkRDFtypeIdent (newVar v) c;
    That    c v = mkRDFtypeIdent (newVar v) c;
    These   c v = mkRDFtypeIdent (newVar v) c;
    Those   c v = mkRDFtypeIdent (newVar v) c;

    ---- Coordination ----

    --BaseIndividual{{^lite}} _{{/lite}} i1 i2 = mkListNP i1 i2; 
    --ConsIndividual{{^lite}} _{{/lite}} i1 i2 = mkListNP i1 i2;  
    --AndIndividual {{^lite}} _{{/lite}} i     = mkNP and_Conj i; 
    --OrIndividual  {{^lite}} _{{/lite}} i     = mkNP or_Conj  i;

    ---- Modification ----

    --modify_P c  p  = { cn = variants { mkCN c.cn (mkRS (mkRCl which_RP p.vp)); 
    --                                   mkCN c.cn (mkRS (mkRCl which_RP (mkClSlash p.np p.vpSlash))) };
    --                   ap  = NONE };
    --modify_C c1 c2 = { cn = variants { mkCN c2.ap c1.cn;
    --                                   mkCN c1.cn (mkRS (mkRCl which_RP (mkVP c2.cn))); 
    --                                   mkCN c1.cn (mkRS (mkRCl which_RP (mkVP c2.ap))) };
    --                   ap  = NONE };  

}
