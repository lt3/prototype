
abstract Core_Datatypes = Numeral, Core ** {


 fun

    ---- Booleans ----

    xsd_boolean : Datatype;
    xsd_true    : Literal{{^lite}} xsd_boolean{{/lite}};
    xsd_false   : Literal{{^lite}} xsd_boolean{{/lite}};
    
    ---- Numerals ----

    xsd_integer            : Datatype;
    xsd_positiveInteger    : Datatype;
    xsd_nonNegativeInteger : Datatype;
    xsd_double             : Datatype;
    xsd_float              : Datatype;
    xsd_decimal            : Datatype;

    literal_1 : Numeral -> Literal{{^lite}} xsd_integer{{/lite}};
    literal_2 : Numeral -> Literal{{^lite}} xsd_positiveInteger{{/lite}};
    literal_3 : Numeral -> Literal{{^lite}} xsd_nonNegativeInteger{{/lite}};

    ---- Unit datatypes (e.g. grams, kilometers) ----

    literal_unit : Numeral -> {{^lite}}(d : {{/lite}}Datatype{{^lite}}){{/lite}} -> Literal{{^lite}} d{{/lite}};

    ---- Strings ----

    xsd_string  : Datatype;

    ---- Dates and times ----

    xsd_date       : Datatype;
    xsd_dateTime   : Datatype;
    xsd_gYear      : Datatype;
    xsd_gYearMonth : Datatype;
    
    ---- URIs ----

    xsd_anyURI : Datatype;

}
