--# -path=.:prelude:{{path}}

concrete CoreGer of Core = CoreI with 

   (Basic  = BasicGer),
   (Syntax = SyntaxGer), 
   (Extra  = ExtraGer) ** open SyntaxGer, ParadigmsGer in { 

  lin NamedIndividual{{^lite}} _{{/lite}} = mkNP (mkPN "XXX");

  oper
     XXX_Nn = mkN "XXX" "XXX" neuter;
     XXX_Nf = mkN "XXX" "XXX" feminine;
     XXX_Nm = mkN "XXX" "XXX" masculine;
} 
