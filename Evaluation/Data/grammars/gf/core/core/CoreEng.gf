--# -path=.:prelude:{{path}}

concrete CoreEng of Core = CoreI with 

   (Basic  = BasicEng),
   (Syntax = SyntaxEng), 
   (Extra  = ExtraEng) ** open SyntaxEng, ParadigmsEng in { 

  lin NamedIndividual{{^lite}} _{{/lite}} = mkNP (mkPN "XXX");
}
