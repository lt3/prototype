
resource SPARQL = open Prelude in {
	
    param QueryType = Select | Ask | Unspec;
    param Modifier  = Count        | None;

    param Var = V0 | V1 | V2 | V3 | V4 | V5 | V6 | V7 | V8 | V9 | Null ;

    oper

        varNext : Var -> Var = \ v -> case v of {
                  V0 => V1; V1 => V2; V2 => V3; V3 => V4; V4 => V5;
                  V5 => V6; V6 => V7; V7 => V8; V8 => V9; _  => V0 } ;

        ---- Linearization types ----
        
        Ident  = { ident  :  Str;  conditions : Query; projecting : Bool; modifier : Modifier };
      --Idents = { idents : [Str]; conditions : Query };

        Triple = { s  : Str; p : Str; o : Str };

        Query  = { queryType   : QueryType;
                   vars        : Str;
                   body        : Str };

        ---- Constants ----

        rdf_type = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";

        ---- Triples ---- 

        mkTriple : Str -> Str -> Str -> Triple = \ s,p,o -> { s = s; p = p; o = o }; 
        flattenTriple : Triple -> Str = \ t -> t.s ++ t.p ++ t.o ++ ".";

        ---- Constructing queries ---- 

        emptyQuery : Query = { queryType = Unspec; vars = ""; body = "" }; 

        mkQuery = overload {
        mkQuery : Triple -> Query = \ t ->
                      { queryType = Unspec;
                             vars = "";
                             body = flattenTriple t }
        };

        mkSelectQuery = overload {
        mkSelectQuery : Str -> Query = \ s -> 
                         { queryType = Select;
                                vars = "*";
                                body = "{" ++ "?s" ++ "?p" ++ s ++"." ++ "}" 
                                    ++ "UNION" ++ "{" ++ s ++ "?p" ++ "?o" ++ "}" };
        mkSelectQuery : Query -> Query = \ q -> 
                         { queryType = Select;
                                vars = q.vars;
                                body = q.body };
        mkSelectQuery : Ident -> Query = \ i ->
                         { queryType = Select;
                                vars = case i.modifier of {
                                            Count => "COUNT" ++ "(" ++ i.ident ++ ")"; 
                                            None  => i.ident };
                                body = flatten i.conditions };
        };

        mkQueryFromTriple : Ident -> Str -> Ident -> Query = \ s,p,o ->
                     { queryType = Unspec;
                            vars = case s.projecting of {
                            	        True  => case o.projecting of {
                                                      True  => s.ident ++ o.ident;
                                                      False => s.ident };
                                        False => case o.projecting of {
                                        	          True  => o.ident;
                                                      False => "" } }; 
                            body = s.conditions.body ++ o.conditions.body ++ s.ident ++ p ++ o.ident ++ "."};
        
        ---- Constructing idents ----

        newVar : Var -> Var = \ v -> varNext v;

        count : Str -> Str = \ s -> "COUNT" ++ "(" ++ s ++ ")"; 

        mkIdent = overload {
        mkIdent : Str -> Query -> Ident = \ s,q -> 
                                { ident = s; 
                             conditions = q;
                             projecting = False;
                               modifier = None };
        mkIdent : Bool -> Str -> Query -> Ident = \ b,s,q -> 
                                { ident = s; 
                             conditions = q;
                             projecting = b;
                               modifier = None };
        mkIdent : Bool -> Modifier -> Str -> Query -> Ident = \ b,m,s,q -> 
                                { ident = s; 
                             conditions = q;
                             projecting = b;
                               modifier = m };
        };
        mkRDFtypeIdent : Var -> Str -> Ident = \ v,c ->
                                { ident = showVar v;
                             conditions = { queryType = Unspec;
                                                 vars = "";
                                                 body = flattenTriple { s = showVar v; p = rdf_type; o = c } };
                             projecting = False;
                               modifier = None };

        flatten : Query -> Str = \ q -> (start q) ++ "WHERE" ++ "{" ++ q.body ++ "}";
        
        start   : Query -> Str = \ q -> case q.queryType of {
                                             Ask    => "ASK";
                                             Select => "SELECT" ++ q.vars;
                                             Unspec => "SELECT" ++ "*" };

        showVar : Var -> Str = \ v -> case v of {
                  V1 => "?v1"; V2 => "?v2"; V3 => "?v3"; V4 => "?v4"; V5 => "?v5"; 
                  V6 => "?v6"; V7 => "?v7"; V8 => "?v8"; V9 => "?v9"; _  => "?v0" } ;

}
