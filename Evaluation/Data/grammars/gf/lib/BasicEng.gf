--# -path=.:prelude:{{path}}

instance BasicEng of Basic = open Prelude, SyntaxEng, ParadigmsEng in {

 oper

    -- Dummies

    NONE = overload {	
      NONE : A  = mkA  "DUMMY";
      NONE : A2 = mkA2 (mkA "DUMMY") noPrep;
      NONE : N  = mkN  "DUMMY";
      NONE : N2 = mkN2 "DUMMY";
      NONE : V  = mkV  "DUMMY";
      NONE : V2 = mkV2 "DUMMY";
      NONE : Prep = noPrep;
      NONE : Adv  = ParadigmsEng.mkAdv "DUMMY";
      NONE : Cl = mkCl (mkN "DUMMY");
      NONE : CN = mkCN (mkN "DUMMY");
      NONE : AP = mkAP (mkA "DUMMY");
      NONE : NP = mkNP (mkN "DUMMY");
      NONE : VP = mkVP (mkV "DUMMY");
      NONE : SC = mkSC (mkVP (mkV "DUMMY"));
      NONE : VPSlash = mkVPSlash (mkV2 (mkV "DUMMY"));
      NONE : ClSlash = mkClSlash (mkNP (mkN "DUMMY")) (mkV2 (mkV "DUMMY"));
    };

}
