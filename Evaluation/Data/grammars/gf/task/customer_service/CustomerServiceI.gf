--# -path=.:prelude:{{path}}

incomplete concrete CustomerServiceI of CustomerService = ClausesI ** open Basic, Syntax in {

 lin

    I          = i_NP;
    You        = you_NP; 
    Poss_I   c = mkNP i_Pron c.cn;
    Poss_You c = mkNP youSg_Pron c.cn;

    GiveMe  {{^lite}}_{{/lite}} i = variants { mkUtt (mkImp (mkVP give_V3 i_NP i)); 
                                               mkUtt (mkImp (mkVP show_V3 i_NP i));
                                               mkUtt (mkImp (mkVP list_V2 i)) };

    Need  d {{^lite}}_{{/lite}} i = mkS (mkCl d (mkVP need_V2 i));
    Want  d {{^lite}}_{{/lite}} i = variants { mkS (mkCl d (mkVP want_V2 i));
                                               {{#alltenses}}mkIWouldLike i;{{/alltenses}}
                                             };
    WantTo d p = variants { mkS (mkCl d (mkVP want_VV p.vp));
              {{#alltenses}}mkIWouldLike p.vp;{{/alltenses}}
                          };

    DoYouHave_Class                          c = mkUtt (mkQS (mkCl you_NP (mkVP have_V2 (mkNP aPl_Det c.cn))));
    DoYouHave_Individual {{^lite}}_{{/lite}} i = mkUtt (mkQS (mkCl you_NP (mkVP have_V2 i)));

}
