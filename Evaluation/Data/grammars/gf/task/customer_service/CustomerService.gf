
abstract CustomerService = Clauses ** {

 fun

    DialogPartner    : Class;
    I, You           : Individual {{^lite}}DialogPartner{{/lite}};
    Poss_I, Poss_You : {{^lite}}(c : {{/lite}}Class{{^lite}}){{/lite}} -> Individual {{^lite}}c{{/lite}};

    GiveMe  : {{^lite}}(c : Class) ->{{/lite}} Individual {{^lite}}c{{/lite}} -> Utterance_Utt;

    Need    : Individual {{^lite}}DialogPartner{{/lite}} -> {{^lite}}(c : Class) ->{{/lite}} Individual {{^lite}}c{{/lite}} -> Sentence_S;
    Want    : Individual {{^lite}}DialogPartner{{/lite}} -> {{^lite}}(c : Class) ->{{/lite}} Individual {{^lite}}c{{/lite}} -> Sentence_S;
    WantTo  : Individual {{^lite}}DialogPartner{{/lite}} -> Statement -> Sentence_S;

    DoYouHave_Class      : Class -> Utterance_Utt;
    DoYouHave_Individual : {{^lite}}(c : Class) ->{{/lite}} Individual {{^lite}}c{{/lite}} -> Utterance_Utt;

    -- How about ...? 
    -- I would like to know the ... you have ...
    -- What does ... mean? What does ... stand for?
    
}
