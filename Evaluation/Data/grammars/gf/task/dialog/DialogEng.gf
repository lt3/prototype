--# -path=.:prelude:{{path}}

concrete DialogEng of Dialog = ClausesEng, GreetingEng ** DialogI with 
                     (Basic  = BasicEng),
                     (Syntax = SyntaxEng) ** {

 lin

  Thanks, ThanksPol = variants { mkDialogAct "thank you"; mkDialogAct "thanks" };
  ReThanks          = variants { mkDialogAct "my pleasure"; mkDialogAct "no problem" };

  Sorry = mkDialogAct "sorry";

  Confirm    = variants { yes_Utt; mkDialogAct "ok"; mkDialogAct "sure" };
  Disconfirm = variants { no_Utt;  mkDialogAct "no thanks" };

}