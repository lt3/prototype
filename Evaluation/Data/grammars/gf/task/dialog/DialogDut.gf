--# -path=.:prelude:{{path}}

concrete DialogDut of Dialog = ClausesDut, GreetingDut ** DialogI with 
                     (Basic  = BasicDut),
                     (Syntax = SyntaxDut) ** {

 lin

  Thanks    = variants { mkDialogAct "dank je"; mkDialogAct "dank je wel"; mkDialogAct "bedankt" };
  ThanksPol = variants { mkDialogAct "dank u"; mkDialogAct "dank u wel" };
  ReThanks  = variants { mkDialogAct "graag gedaan"; mkDialogAct "alstublieft"; mkDialogAct "geen problem" };

  Sorry     = variants { mkDialogAct "pardon"; mkDialogAct "sorry" };

  Confirm    = variants { yes_Utt; mkDialogAct "oke" };
  Disconfirm = variants { no_Utt;  mkDialogAct "nee hoor" };

}