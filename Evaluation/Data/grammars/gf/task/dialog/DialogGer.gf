--# -path=.:prelude:{{path}}

concrete DialogGer of Dialog = ClausesGer, GreetingGer ** DialogI with 
                     (Basic  = BasicGer),
                     (Syntax = SyntaxGer) ** {

 lin

  Thanks    = mkDialogAct "danke"; 
  ThanksPol = variants { mkDialogAct "vielen dank"; mkDialogAct "dankeschoen" };
  ReThanks  = variants { mkDialogAct "gern geschehen"; mkDialogAct "bitte"; mkDialogAct "keine ursache" };

  Sorry     = variants { mkDialogAct "entschuldigung"; mkDialogAct "tut mir leid" };

  Confirm    = variants { yes_Utt; mkDialogAct "ok" };
  Disconfirm = variants { no_Utt;  mkDialogAct "nein danke" };

}