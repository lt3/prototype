--# -path=.:prelude:{{path}}

incomplete concrete DialogI of Dialog = open Basic, Syntax in {

 lincat 

    DialogAct     = Utt;
    DialogPartner = NP;

 lin

    ---- Dialog partners ----

    I      = i_NP;
    You    = you_NP;
    YouPol = youPol_NP;
    YouPl  = youPl_NP;

    Poss_I      c = mkNP i_Pron      c.cn;
    Poss_You    c = mkNP youSg_Pron  c.cn;
    Poss_YouPol c = mkNP youPol_Pron c.cn;
    Poss_YouPl  c = mkNP youPl_Pron  c.cn;

    ---- Dialog acts ----

    greet    g = g;
    
    question q = mkUtt q;
    inform   s = mkUtt s;

}
