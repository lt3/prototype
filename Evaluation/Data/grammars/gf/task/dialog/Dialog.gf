
abstract Dialog = Clauses, Greeting ** {

 cat

    DialogAct;
    DialogPartner;

fun 
 
    ---- Dialog partners ----

    I, You, YouPol, YouPl : DialogPartner;
    Poss_I, Poss_You, Poss_YouPol, Poss_YouPl : {{^lite}}(c : {{/lite}}Class{{^lite}}){{/lite}} -> Individual {{^lite}}c{{/lite}};

    ---- Dialog acts ----

    greet    : Greeting -> DialogAct;

    question : Question_QS -> DialogAct;
    inform   : Sentence_S  -> DialogAct;

    Thanks, ThanksPol, ReThanks : DialogAct;
    Sorry    : DialogAct;

    Confirm, Disconfirm : DialogAct;

}
