--# -path=.:prelude:{{path}}

incomplete concrete DocumentationI of Documentation = ClausesI ** open Basic, Syntax in {

 lincat

   Phrase_Phr    = Phr;
   Documentation = Text;

 lin

   Because s1 s2 = mkClause s1 mkClause s2;

   But       s = mkPhr but_PConj       (mkUtt (mkS (mkClause s)));
   Therefore s = mkPhr therefore_PConj (mkUtt (mkS (mkClause s)));

}
