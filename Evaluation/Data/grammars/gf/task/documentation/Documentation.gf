
abstract Documentation = Clauses ** {

 cat 

   Phrase_Phr;
   Documentation;

 fun 

   Because : Statement -> Statement -> Phrase_Phr;
   
   But, Therefore : Statement -> Phrase_Phr;

}
