--# -path=.:prelude:{{path}}

concrete DateEng of Date = NumeralEng, DateI ** {

 lin
 
    MJan = "January";
    MFeb = "February";
    MMar = "March";
    MApr = "April";
    MMay = "May";
    MJun = "June";
    MJul = "July";
    MAug = "August";
    MSep = "September";
    MOct = "October";
    MNov = "November";
    MDec = "December";

    WSun = "Sunday";
    WMon = "Monday";
    WTue = "Tuesday";
    WWed = "Wednesday";
    WThu = "Thursday";
    WFri = "Friday";
    WSat = "Saturday";

}
