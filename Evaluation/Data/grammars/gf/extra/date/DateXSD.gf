--# -path=.:prelude:{{path}}

concrete DateXSD of Date = NumeralApp, DateI ** {

 lin

    MkTimePoint y m d t = y ++ "-" ++ m ++ "-" ++ d ++ t; -- ss (...)

    MJan = "01" ;
    MFeb = "02" ;
    MMar = "03" ;
    MApr = "04" ;
    MMay = "05" ;
    MJun = "06" ;
    MJul = "07" ;
    MAug = "08" ;
    MSep = "09" ;
    MOct = "10" ;
    MNov = "11" ;
    MDec = "12" ;

}
