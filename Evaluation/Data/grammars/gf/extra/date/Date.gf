-- Based on date grammar by Kaarel Kaljurand (https://github.com/Kaljurand/Grammars/gf/Date)
-- and the Time Ontology by Hobbs & Pan.

abstract Date = Numeral ** {
	
 cat

    Year ; Month ; Day ; Timestamp ; Hour ; Minute ; Weekday ;
    
    TimeUnit;
    TimePoint;
    TimeInterval TimeUnit;


 fun
  
    UDay, UWeek, UMonth, UYear, UDecade : TimeUnit;

    SmallNum : Sub1000 -> Year;

    MkTimestamp : Hour -> Minute -> Timestamp;
    MkTimePoint : Year -> Month -> Day -> Timestamp -> TimePoint;

    MJan, MFeb, MMar, MApr, MMay, MJun, MJul, MAug, MSep, MOct, MNov, MDec : Month;

    D00, D01, D02, D03, D04, D05, D06, D07, D08, D09, 
    D10, D11, D12, D13, D14, D15, D16, D17, D18, D19,
    D20, D21, D22, D23, D24, D25, D26, D27, D28, D29,
    D30, D31 : Day ;
 
    H00, H01, H02, H03, H04, H05, H06, H07, H08, H09, H10, H11, H12,
    H13, H14, H15, H16, H17, H18, H19, H20, H21, H22, H23 : Hour ;

    M00, M01, M02, M03, M04, M05, M06, M07, M08, M09,
    M10, M11, M12, M13, M14, M15, M16, M17, M18, M19,
    M20, M21, M22, M23, M24, M25, M26, M27, M28, M29,
    M30, M31, M32, M33, M34, M35, M36, M37, M38, M39,
    M40, M41, M42, M43, M44, M45, M46, M47, M48, M49,
    M50, M51, M52, M53, M54, M55, M56, M57, M58, M59 : Minute ;

    WSun, WMon, WTue, WWed, WThu, WFri, WSat : Weekday ;

    -- noncanonical ways

    Now : Timestamp;
    DToday, DYesterday, DTomorrow : Timestamp -> TimePoint;
    DLast, DNext : (u : TimeUnit) -> Timestamp -> TimeInterval u;
    DIn, DAgo : Numeral -> TimeUnit -> TimePoint;
}
