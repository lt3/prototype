--# -path=.:prelude:present

incomplete concrete CoreI of Core = open Basic, Syntax, Extra in {


 lincat

    Class       = ClassRecord;
    Datatype    = CN;

    Individual  = NP;
   [Individual] = [NP];

    Literal     = NP;

    Statement   = StatementRecord;
 

 lin   

    ---- Determiners ----

    The     c = mkNP the_Det c.cn;
    Some    c = mkNP aSg_Det c.cn;
    Most    c = mkNP most_Predet (mkNP aPl_Det c.cn);
    All     c = variants { mkNP all_Predet (mkNP aPl_Det c.cn);
                           mkNP every_Det c.cn }; 

    Generic c = mkNP aPl_Det c.cn; 

    This  c = mkNP this_Det  c.cn;
    That  c = mkNP that_Det  c.cn;
    These c = mkNP these_Det c.cn;
    Those c = mkNP those_Det c.cn;


    ---- Anaphora ----

    He    = he_NP;
    She   = she_NP;
    It    = it_NP;
    They  = they_NP; 

    Poss_He   c = mkNP he_Pron   c.cn;
    Poss_She  c = mkNP she_Pron  c.cn;
    Poss_It   c = mkNP it_Pron   c.cn;
    Poss_They c = mkNP they_Pron c.cn;


    ---- Coordination ----

    BaseIndividual i1 i2 = mkListNP i1 i2; 
    ConsIndividual i1 i2 = mkListNP i1 i2;  
    AndIndividual  i     = mkNP and_Conj i; 
    OrIndividual   i     = mkNP or_Conj  i;


    ---- Modification ----

    modify_SC c  p  = { cn  = variants { mkCN c.cn (mkRS (mkRCl which_RP p.vp)); 
                                         mkCN c.cn (mkRS (mkRCl which_RP (mkClSlash p.np p.vpSlash))) };
                        ap  = NONE; 
                        adv = NONE };
    modify_SS c  p  = { np  = p.np;
                        vp  = mkVP p.vp c.adv; 
                        vpSlash = p.vpSlash };
    modify_CC c1 c2 = { cn  = variants { mkCN c2.ap c1.cn;
                                         mkCN c1.cn (mkRS (mkRCl which_RP (mkVP c2.cn))); 
                                         mkCN c1.cn (mkRS (mkRCl which_RP (mkVP c2.ap))) };
                        ap  = NONE;
                        adv = NONE }; 


    ---- Semantically light expressions ----

    toBe    i1 i2 = { np = i1; vp = mkVP i2; vpSlash = NONE };
    toHave  i1 i2 = { np = i1; vp = mkVP have_V2 i2; vpSlash = mkVPSlash have_V2 };


}
