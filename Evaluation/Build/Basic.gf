--# -path=.:prelude:present

interface Basic = open Prelude, Syntax in {

  oper 

    -- Records

    ClassRecord     = { cn : CN; ap : AP; adv : Adv };
    StatementRecord = { np : NP; vp : VP; vpSlash : VPSlash };


    -- Construction methods

    mkClass = overload {
    mkClass : CN  -> ClassRecord = \ cn  -> { cn = cn;   ap = NONE; adv = NONE };
    mkClass : AP  -> ClassRecord = \ ap  -> { cn = NONE; ap = ap;   adv = NONE };
    mkClass : Adv -> ClassRecord = \ adv -> { cn = NONE; ap = NONE; adv = adv };
    mkClass : CN  -> AP -> ClassRecord = \ cn,ap -> { cn = cn; ap = ap; adv = NONE };
    }; 

    mkIndividual = overload {
    mkIndividual : NP -> NP = \np -> np;
    mkIndividual : N  -> NP = \n  -> mkNP the_Quant n; 
    mkIndividual : CN -> NP = \cn -> mkNP the_Quant cn; 
    mkIndividual : PN -> NP = \pn -> mkNP pn; 
    };

    mkStatement = overload {
    mkStatement : NP -> VP -> VPSlash -> StatementRecord = \ np,vp,vpSlash ->
                { np = np; vp = vp; vpSlash = vpSlash };
    mkStatement : NP -> VP -> StatementRecord = \ np,vp ->
                { np = np; vp = vp; vpSlash = NONE };                
    mkStatement : NP -> CN -> StatementRecord = \ np,cn ->
                { np = np;  vp = mkVP cn; vpSlash = NONE };
    mkStatement : NP -> Adv -> StatementRecord = \ np,adv ->
                { np = np; vp = mkVP adv; vpSlash = NONE };

    -- for robustness
    mkStatement : VP -> StatementRecord = \ vp ->
                { np = it_NP; vp = vp; vpSlash = NONE };

    -- for overlap with mkClass
    mkStatement : CN -> StatementRecord = \ cn ->
                { np = NONE; vp = mkVP cn; vpSlash = NONE };
    mkStatement : AP -> StatementRecord = \ ap ->
                { np = NONE; vp = mkVP ap; vpSlash = NONE };
    mkStatement : Adv -> StatementRecord = \ adv ->
                { np = NONE; vp = mkVP adv; vpSlash = NONE };
    mkStatement : CN -> AP -> StatementRecord = \ cn,ap ->
                { np = NONE; vp = variants {mkVP cn; mkVP ap}; vpSlash = NONE };
    };

    -- Sentence-level

    mkClause : StatementRecord -> Cl = \ p -> mkCl p.np p.vp;

    -- Dialog-related  

    mkDialogAct : Str -> Utt = \ s -> lin Utt (ss s);

}
