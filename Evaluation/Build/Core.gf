
abstract Core = {


 cat
  
    Class;
    Datatype;

    Individual ;
    [Individual ] {2};

    Literal;

    Statement;


 fun 


    ---- Determiners ----

    The, Some, All, Most, Generic, 
    This, That, These, Those : Class -> Individual ;


    ---- Anaphora ----

    He, She, It, They  :  Individual ; 
    Poss_He, Poss_She, 
    Poss_It, Poss_They : Class -> Individual ;


    ---- Coordination ----

    AndIndividual, OrIndividual :  [Individual] -> Individual; 


    ---- Modification ----

    modify_SC : Class -> Statement -> Class;
    modify_SS : Class -> Statement -> Statement;
    modify_CC : Class -> Class     -> Class;


    ---- Semantically light expressions ----

    toBe, toHave :  Individual -> Individual -> Statement;

    -- prepostions: with, ...


    ---- Placeholder after NER ----

    NamedIndividual :  Individual;


    ---- OWL stuff to worry about later 

    owl_Thing : Class;

    ---- Booleans ----

    xsd_boolean : Datatype;
    xsd_true    : Literal;
    xsd_false   : Literal;
    
    ---- Numerals ----

    xsd_integer            : Datatype;
    xsd_positiveInteger    : Datatype;
    xsd_nonNegativeInteger : Datatype;
    xsd_double             : Datatype;
    xsd_float              : Datatype;
    xsd_decimal            : Datatype;

    ---- Strings ----

    xsd_string  : Datatype;

    ---- Dates and times ----

    xsd_date       : Datatype;
    xsd_dateTime   : Datatype;
    xsd_gYear      : Datatype;
    xsd_gYearMonth : Datatype;
    
    ---- URIs ----

    xsd_anyURI : Datatype;

}
