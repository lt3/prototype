--# -path=.:prelude:present

incomplete concrete CustomerServiceI of CustomerService = ClausesI ** open Basic, Syntax in {

 lin

    I          = i_NP;
    You        = you_NP; 
    Poss_I   c = mkNP i_Pron c.cn;
    Poss_You c = mkNP youSg_Pron c.cn;

    GiveMe   i = variants { mkUtt (mkImp (mkVP give_V3 i_NP i)); 
                                               mkUtt (mkImp (mkVP show_V3 i_NP i));
                                               mkUtt (mkImp (mkVP list_V2 i)) };

    Need  d  i = mkS (mkCl d (mkVP need_V2 i));
    Want  d  i = variants { mkS (mkCl d (mkVP want_V2 i));
                                               
                                             };
    WantTo d p = variants { mkS (mkCl d (mkVP want_VV p.vp));
              
                          };

    DoYouHave_Class                          c = mkUtt (mkQS (mkCl you_NP (mkVP have_V2 (mkNP aPl_Det c.cn))));
    DoYouHave_Individual  i = mkUtt (mkQS (mkCl you_NP (mkVP have_V2 i)));

}
