--# -path=.:prelude:present

concrete CoreEng of Core = CoreI with 

   (Basic  = BasicEng),
   (Syntax = SyntaxEng), 
   (Extra  = ExtraEng) ** open SyntaxEng, ParadigmsEng in { 

  lin NamedIndividual = mkNP (mkPN "XXX");
}
