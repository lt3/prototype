
abstract CustomerService = Clauses ** {

 fun

    DialogPartner    : Class;
    I, You           : Individual ;
    Poss_I, Poss_You : Class -> Individual ;

    GiveMe  :  Individual  -> Utterance_Utt;

    Need    : Individual  ->  Individual  -> Sentence_S;
    Want    : Individual  ->  Individual  -> Sentence_S;
    WantTo  : Individual  -> Statement -> Sentence_S;

    DoYouHave_Class      : Class -> Utterance_Utt;
    DoYouHave_Individual :  Individual  -> Utterance_Utt;

    -- How about ...? 
    -- I would like to know the ... you have ...
    -- What does ... mean? What does ... stand for?
    
}
