# When life gives you lemons, make GF!

lemon2gf transforms an ontology and a <a href="http://lemon-model.net/">lemon</a> lexicon into a <a href="http://www.grammaticalframework.org">GF</a> grammar.

## Requires

* <a href="https://github.com/RDFLib/rdflib">rdflib</a>
* <a href="https://github.com/defunkt/pystache">pystache</a>
* Core <a href="https://github.com/cunger/grammars">grammar module</a>

## Note

lemon2gf is work in progress, so things are still changing and might not always work as expected.
