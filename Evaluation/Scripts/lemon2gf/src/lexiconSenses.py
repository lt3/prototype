
from copy import deepcopy
from rdflib import Namespace, RDF, RDFS, BNode, URIRef

from utils.utils import *


lemon = Namespace("http://www.monnet-project.eu/lemon#")


def collectSenses(logger,lexicon,reader,query,renaming):

    senses = []

    for entry in query.getResults('entry',str(lexicon)):

        # collect all senses of entry
        for senseURI in reader.collect(entry,lemon.sense,True):

            sense = dict(entries=str(entry))

            # get reference, semantic arguments and domain/range restrictions
            _collectReference(reader,sense,senseURI)
            _collectSenseArgs(reader,sense,senseURI)

            # get subsenses
            sense['subsenses'] = []
            for subsenseURI in reader.collect(senseURI,lemon.subsense,True):
                subsense = dict()
                _collectReference(reader,subsense,subsenseURI)
                _collectSenseArgs(reader,subsense,subsenseURI)
                sense['subsenses'].append(subsense)

            # if it's a complex sense
            if sense['subsenses']:
               complex_sense = _constructComplexSense(sense,reader)
               complex_sense['entries'] = sense['entries']
               reader.blankNodeMap[str(senseURI)] = complex_sense['reference'] 
               senses.append(complex_sense)
            # if it's a simple sense
            else:
               del sense['subsenses']
               senses.append(sense)

    # group entries with the same sense
    new_senses = []

    for s in senses:

        if not s.has_key('reference'):
           logger.missing_reference(str(s))
           continue

        s['entries'] = [s.pop('entries',None)]
        # build renaming dict for sense/entry pair
        _buildRenaming(s,s['entries'],renaming,logger)
        # group
        if not new_senses: new_senses.append(s)
        else: 
           new = True
           for e in new_senses:
               if e['reference'] == s['reference']: 
                  e['entries'].append(s['entries'][0])
                  new = False 
                  break
           if new: new_senses.append(s)

    # process domain and range restrictions
    for s in senses: 
        domain_res = s.has_key('propertyDomain')
        range_res  = s.has_key('propertyRange')
        if domain_res or range_res:
           for r in reader.signature['relations']:
               if r['reference'] == s['reference']:
                  new_r = dict()
                  ref = r['reference']+'_restrictedTo_'
                  if domain_res: ref += 'domain_' + s['propertyDomain']
                  if range_res:  ref += 'range_'  + s['propertyRange']
                  new_r['reference'] = s['reference'] = ref
                  new_r['arguments'] = deepcopy(r['arguments'])
                  if domain_res: new_r['arguments'][0]  = dict(name=s['propertyDomain'])
                  if range_res:  new_r['arguments'][-1] = dict(name=s['propertyRange'])
                  reader.signature['relations'].append(new_r)
                  # build renaming dict for new sense/entry pair
                  _buildRenaming(new_r,s['entries'],renaming,logger)
                  break

    # replace blank node identifiers and transfer linWith + linOverride + module
    for n in new_senses:
        if reader.blankNodeMap.has_key(n['reference']):
           n['reference'] = reader.blankNodeMap[n['reference']]
        for re in referringExpressions(reader.signature):
            if re['reference'] == n['reference']:
               if re.has_key('linWith'):     n['linWith']     = re['linWith']
               if re.has_key('linOverride'): n['linOverride'] = re['linOverride']
               if re.has_key('module'):      n['module']      = re['module']
               break
    # complete sense information if necessary
    for s in referringExpressions(reader.signature):
        if s.has_key('get'):
           for d in s['get']:
               for re in referringExpressions(reader.signature):
                   if re['reference'] == d['where'] and re.has_key(d['what']):
                      s[d['what']] = re[d['what']]
                      break
           del s['get']

    return new_senses


def _collectReference(reader,senseDict,uri):
    if reader.blankNodeMap.has_key(uri):
       reference = [ reader.blankNodeMap[uri] ]
    else:
       reference = reader.collect(uri,lemon.reference)
    if reference:
       s = reader.constructSense(reference[0])
       if s is not None: senseDict.update(s)

def _collectSenseArgs(reader,senseDict,uri):

    # arguments
    addValues(senseDict,'isA',map(lambda x: frag_ident(x,reader.namespaces),reader.collect(uri,lemon.isA)))
    addValues(senseDict,'subjOfProp',map(lambda x: frag_ident(x,reader.namespaces),reader.collect(uri,lemon.subjOfProp)))
    addValues(senseDict,'objOfProp', map(lambda x: frag_ident(x,reader.namespaces),reader.collect(uri,lemon.objOfProp)))
  
    # domain/range restrictions
    propDomain = reader.collect(uri,lemon.propertyDomain)
    propRange  = reader.collect(uri,lemon.propertyRange)
    if len(propDomain) > 1: logger.not_unique(str(uri),'propertyDomain',str(propDomain),'the first one that works')
    for d in propDomain:
        ds = reader.constructSense(d)
        if ds is not None:
           addValues(senseDict,'propertyDomain',ds['reference'])
           break
    if len(propRange)  > 1: logger.not_unique(str(uri),'propertyRange', str(propRange), 'the first one that works')
    for r in propRange:
        rs = reader.constructSense(r)
        if rs is not None:
           addValues(senseDict,'propertyRange',rs['reference'])
           break

def args2list(d):
    args = [] 
    # ignoring 'isA'
    if d.has_key('subjOfProp'):
       if type(d['subjOfProp']) == list: args += d['subjOfProp']
       else: args.append(d['subjOfProp'])
    if d.has_key('objOfProp'):
       if type(d['objOfProp'])  == list: args += d['objOfProp'] 
       else: args.append(d['objOfProp'])
    return args


## CONSTRUCTING COMPLEX SENSES ##

def _constructComplexSense(sense,reader):
# this function is not sexy but it works...
# TODO if there's one unbound argument, the sense is a class with this argument as isA

    new_sense = dict()

    reference    = ''
    isClass      = False
    isAs         = []
    subjOfProps  = [] 
    objOfProps   = []
    superclasses = []
    arg2type     = dict()

    # sense
    if sense.has_key('reference'):  reference = sense['reference']
    if sense.has_key('isA'):        isAs.append(sense['isA']); isClass = True
    if sense.has_key('subjOfProp'): subjOfProps.append(sense['subjOfProp'])
    if sense.has_key('objOfProp'):  objOfProps.append( sense['objOfProp'])
      
    # subsenses  
    for sub in sense['subsenses']:
        reference += '_'+sub['reference']
        # get isA
        if sub.has_key('isA'): 
           isAs.append(sub['isA'])
           superclasses.append(sub['reference'])
        # get subjOfProp and domain
        if sub.has_key('subjOfProp'):
           subj = sub['subjOfProp']
           if isEntity(subj,reader.signature):
              reference += '_of_'+subj
           elif subj not in subjOfProps: 
                subjOfProps.append(subj)
                if sub.has_key('arguments'): 
                   arg2type[subj] = sub['arguments'][0]
        # get objOfProp and range
        if sub.has_key('objOfProp'):
           obj = sub['objOfProp']
           if isEntity(obj,reader.signature):
              reference += '_'+obj
           elif obj not in objOfProps: 
                objOfProps.append(obj)
                if sub.has_key('arguments'): 
                   arg2type[obj] = sub['arguments'][-1]

    subjOfProps = [ x for x in subjOfProps if x not in isAs ]
    objOfProps  = [ x for x in objOfProps  if x not in isAs ]

    addValues(new_sense,'isA',isAs)
    addValues(new_sense,'subjOfProp',subjOfProps)
    addValues(new_sense,'objOfProp',objOfProps)

    if reference.startswith('_'): reference = reference[1:]
    new_sense['reference'] = reference

    if reference:
       # add new sense to signature
       new = dict(reference=reference)
       # arguments
       new['arguments'] = []
       if len(subjOfProps) == 1 and len(objOfProps) == 1: 
          subj = subjOfProps[0] 
          obj  = objOfProps[0]
          if arg2type.has_key(subj): new['arguments'].append(arg2type[subj])
          else:                      new['arguments'].append(dict(name=reader.commonSuper))
          if arg2type.has_key(obj):  new['arguments'].append(arg2type[obj])
          else:                      new['arguments'].append(dict(name=reader.commonSuper))
       else:
          new['arguments'] = map(lambda x: arg2type[x] if arg2type.has_key(x) 
                                           else dict(name=reader.commonSuper),
                                 subjOfProps+objOfProps)
       if isClass: 
          new['superclasses'] = superclasses
          reader.addtoSignature('classes',new)
       else:
          if not new in reader.signature['relations']: 
             reader.addtoSignature('relations',new)

    return new_sense

def _buildRenaming(d,entries,renaming,logger):
    rename = dict()
    args   = args2list(d)
    i = 1
    for arg in args:
        rename[arg] = 'arg'+str(i)
        i += 1
    for e in entries:
        k = (d['reference'],e)
        if renaming.has_key(k):
           intersection = intersect(renaming[k].keys(),rename.keys())
           if intersection and renaming[k] != rename:
              logger.renaming_overlaps(str(k),', '.join(intersection))
           renaming[k].update(rename)
        else: 
           renaming[k] = rename
