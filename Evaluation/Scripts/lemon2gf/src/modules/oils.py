import rdflib as rdf
from   rdflib import RDF, RDFS, Namespace

from utils.RDFreader          import isBlankNode
from utils.utils              import frag_uri
from config.global_parameters import commonSuper



OWL  = Namespace("http://www.w3.org/2002/07/owl#")
XSD  = Namespace("http://www.w3.org/2001/XMLSchema#")
oils = Namespace("http://www.lemon-model.net/oils#")


def collect(reader,lite):
# returns a list of URIs that will be ignored by ontology2abstract

    reader.ignore += _collectScalars(reader,commonSuper(lite))


def _collectScalars(reader,commonSuper):

    uris = []

    for s,_,_ in reader.graph.triples((None, RDFS.subClassOf, oils.CovariantScalar)):
        if s not in uris:
           uris.append(s)
           prop = None
           degr = None
           for _,_,o in reader.graph.triples((s, oils.boundTo, None)):
               prop = reader.constructProperty(o)
               break
           for _,_,o in reader.graph.triples((s, oils.degree, None)):
               degr = frag_uri(o,reader.namespaces)
               break
           if prop is not None and degr is not None:
              ref = 'Things_with_'+degr+'_'+prop['reference']
              reader.signature['classes'].append(dict(reference=ref,superclasses=[commonSuper]))
              if isBlankNode(s):
                 reader.blankNodeMap[s] = ref

    return uris

