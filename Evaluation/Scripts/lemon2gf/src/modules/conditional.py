import rdflib as rdf
from   rdflib import RDF, RDFS, Namespace

from utils.utils import frag_uri


OWL = Namespace("http://www.w3.org/2002/07/owl#")
XSD = Namespace("http://www.w3.org/2001/XMLSchema#")



def collect(reader):
# returns a list of URIs that will be ignored by ontology2abstract

    reader.ignore += _collectStates(reader)
    reader.ignore += _collectPreconditions(reader)


def _collectStates(reader):

    uris = []

    #reader.signature['hascats']    = True
    #reader.signature['haslincats'] = True
    #reader.signature['cats'].append('State Class')
    #reader.signature['lincats'].append(dict(cat='State',lincat='ClassRecord'))

    states = []
    State = reader.guessURI('State')
    if State is not None: 
       uris.append(State)
       for s,_,_ in reader.graph.triples((None, RDFS.subClassOf, rdf.URIRef(State))):
           if s not in states:
              uris.append(s)
              reader.signature['relations'].append(dict(reference  = frag_uri(s,reader.namespaces),
                                                        arguments  = [ dict(name='(c : Class)',datatype=True), dict(name='c') ],
                                                        subjOfProp = '_',
                                                        objOfProp  = 'subject',
                                                        returntype = 'Individual c',
                                                        linWith    = 'mkStatement'))
              states.append(s)

    return uris

def _collectPreconditions(reader):

    uris = []

    precondition = reader.guessURI('precondition')
    if precondition is not None: 
       uris.append(precondition)
       for s,_,_ in reader.graph.triples((None, RDFS.subPropertyOf, rdf.URIRef(precondition))):
           uris.append(s)
           p = reader.constructProperty(s)
           if p is not None:
              #p['arguments'][-1] = dict(name = 'State '+ p['arguments'][-1]['name'], datatype = True )
              p['returntype'] = 'Sentence'
              reader.signature['relations'].append(p)

    return uris

