
resource ConditionalEng = open BasicEng, SyntaxEng, ParadigmsEng in {

 oper 

    mkConditional : Cl -> Cl -> PropositionRecord = \ c1,c2 -> 
                   { np = NONE;
                     cl = variants {
                          mkS if_then_Conj (mkS c1) (mkS conditionalTense c2);
                          mkS (SyntaxEng.mkAdv if_Subj (mkS c1) (mkS conditionalTense c2) }; 
                      --  mkS (mkConj "unless") (mkS negativePol (mkS c2)) (mkS c1) } ;

}
