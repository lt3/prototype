
from os.path import exists
from os      import listdir
import traceback
import codecs

import re
import pystache

from copy      import deepcopy
from itertools import permutations

from utils.RDFload import *
from utils.utils   import *
from lexiconSenses import collectSenses, args2list
from lexiconForms  import *
from config.gf_specifics import GFify

from utils.RDFreader import RDFreader
from utils.RDFquery  import RDFquery
from moduleHandler   import readModules

import config.global_parameters as config

# SPARQL paths specific to linguistic ontology
sparql_path_ling    = lambda x: 'sparql/'+config.linguistic_ontology+'/'+x+'.sparql'
sparql_subpath_dir  = lambda x: 'sparql/'+config.linguistic_ontology+'/'+x
sparql_subpath_ling = lambda y: lambda x: 'sparql/'+config.linguistic_ontology+'/'+y+'/'+x+'.sparql'



def convert_lexica(reader,lexica,gf_libs,modules,prefix,lite):
  
  mustache = pystache.Renderer(escape=lambda x: x)

  lexicalizations    = dict()
  lexicalized_senses = []

  logger          = reader.logger
  ontology_graph  = reader.graph

  ## FOR ALL LEXICON FILES
  for lexicon_file in lexica:

    # load RDF file
    logger.info('Loading: ' + lexicon_file)
    reader.graph = loadGraph(ontology_graph,lexicon_file)
    readModules(reader,modules,lite)
  
  query = RDFquery(logger,reader.graph,'sparql/')

  for lexicon in query.getResults('lexicon'):
        logger.info('--> Lexicon URI: ' + str(lexicon))

        # get lexicon language and map it to GF language identifier
        langs = query.getResults('language',str(lexicon))
        if len(langs) == 1:
           l = str(langs[0])
           if l in gf_libs.keys(): 
               language = gf_libs[l]
           else: 
               language = l 
               logger.no_resource_grammar(l)
        else: 
               logger.wrong_number_of_languages(lexicon_file)

        # lexicon prefix
        lexPrefix = str(lexicon).replace(frag_uri(str(lexicon),reader.namespaces),'') 
        reader.namespaces[lexPrefix] = prefix+'lex'+language

        renaming = dict()
        # build sense records from lexicon entries
        records = construct_records(logger,lexicon,reader,query,renaming,language)
        logger.printRecords(records)

        # build linearizations and opers

        lins  = []
        opers = []

        for r in records:
            lexicalized_senses.append(r['reference'])
            # if linOverride, then use this and skip all the rest
            if r.has_key('linOverride'):
               lins.append(r['linOverride'])
               continue
            # build arguments
            args = args2list(r)
            arguments = ''; i = 1
            for arg in args:
                arguments += 'arg'+str(i)+' '; i += 1
            # construct linearization dict
            ref = r['reference']
            l = dict(reference=ref,arguments=arguments,lin=[])
            if r.has_key('linWith'):  l['oper']   = r['linWith']
            elif reader.isClass(ref): l['oper']   = 'mkClass'
            elif reader.isProperty(ref):
                 if r.has_key('returntype'): l['oper'] = 'mk'+r['returntype']
                 else:                       l['oper'] = 'mkStatement'
            else:                            l['oper'] = 'mkIndividual'
            for e in r['entries']:
                e['lang']      = language
                e[language]    = True
                e['reference'] = ref
                e['oper']      = l['oper']
                # render relevant templates based on pos and synBehavior
                if e['synBehaviors']:
                   for syn in e['synBehaviors']:
                       fallback = True
                       # get frames
                       frames = []
                       if syn.has_key('frame'):
                          if type(syn['frame']) == list: 
                             frames = filter(lambda x: x != 'Frame',syn['frame'])
                          elif syn['frame'] != 'Frame':
                             frames = [syn['frame']]
                       # render frames
                       for frame in frames:
                          t = 'templates/frames/'+frame+'.mustache'
                          if exists(t): fallback = False
                          else: 
                             # if r.has_key('module'): 
                                t = 'modules/'+frame+'.mustache' # TODO finally adapt to structure of modules folder
                                if exists(t): fallback = False
                          if fallback:
                             logger.no_template(frame) 
                             continue
                          context = e # dict(reference=e['reference'],gf_name=e['gf_name'],canonicalForm=e['canonicalForm'])
                          context.update(syn)
                          uninst  = _uninstantiated(t,context)
                          if uninst: 
                             logger.uninstantiated_fields(str(t.replace('templates/','')),str(uninst),str(e['reference']))
                          try: 
                             result = mustache.render(open(t,'r').read(),context)
                             _collect_lins_opers(result,l['lin'],opers,prefix) 
                          except (KeyError,IOError,OSError):
                             logger.oops(str(e),traceback.format_exc())
                if not e['synBehaviors'] or fallback:
                     pos_n_types = []
                     temps       = []
                     if e.has_key('pos'): 
                        pos_n_types.append(e['pos'])
                        t = 'templates/pos/'+e['pos']+'.mustache'
                        if exists(t): temps.append(t)
                     if e.has_key('type'):
                        pos_n_types.append(', '.join(e['type']))
                        for typ in e['type']:
                            t = 'templates/pos/'+typ+'.mustache'
                            if exists(t): temps.append(t)
                     yay = False
                     for temp in temps:
                         try: 
                            result = mustache.render(open(temp,'r').read(),e)
                            _collect_lins_opers(result,l['lin'],opers,reader.signature['domain_name']+language) 
                            yay = True; break
                         except (KeyError,IOError,OSError):
                            logger.oops(str(e),traceback.format_exc())
                            continue
                     if not yay:
                        logger.pos_not_found(str(pos_n_types),str(e['reference']))
        
            # for pretty printing                          
            if len(l['lin']) > 1: l['variants'] = True    
            # collect resulting linearizations in lins
            lins.append(l)

        # add subclass coercions
        for c in reader.signature['classes']:
            for sc in c['superclasses']:
                coerc = dict(reference='coerce_'+c['reference']+'_to_'+sc,arguments='i',lin=['i'])
                if coerc not in lins: lins.append(coerc)

        # collect linearizations and opers in lexicalizations    
        haslins = False; hasopers = False
        if lins:  haslins = True
        if opers: hasopers = True
        if lexicalizations.has_key(language):
           lexicalizations[language]['lins'].extend(lins)
           lexicalizations[language]['opers'].extend(opers)
        else:
           lexicalizations[language] = { 'lins': lins, 'opers': opers } 

  ## END FOR ALL LEXICON FILES

  ## FOR ALL COLLECTED LEXICALIZATIONS
  # render concrete template as $domain_name$language.gf
  for language_key in lexicalizations.keys(): 

      lexx = dict() 
      # "I am the Lexx. The most powerful weapon of destruction in the two universes."
      lexx['lang']          = language_key
      lexx['domain_name']   = reader.signature['domain_name']
      lexx['modules'] = map(lambda x: x.capitalize(),modules)
      lexx['lins']    = lexicalizations[language_key]['lins']
      lexx['opers']   = lexicalizations[language_key]['opers']
      if lexx['lins']:  lexx['haslins']  = 'true'
      if lexx['opers']: lexx['hasopers'] = 'true'

      concrete = mustache.render(open('templates/concrete.mustache','r').read(),lexx) 
      target = '../test/target/'+reader.signature['domain_name']+language_key+'.gf'
      codecs.open(target,'w','utf-8').write(concrete)
      
      logger.setConcrete(target)

  return lexicalized_senses
  ## DONE


def construct_records(logger,lexicon,reader,query,renaming,lang):

    senses = collectSenses(logger,lexicon,reader,query,renaming)
    addLexicalInformation(logger,reader,query,senses,renaming,lang)
    ignoreClassArgs(senses) # This hack is dedicated to John.
        
    return senses


def ignoreClassArgs(senses):
    for s in senses:
        if s.has_key('isA'): 
           for e in s['entries']:
               for k,v in e.items():
                   if v == s['isA']: 
                      del e[k] 
                      break
           del s['isA']




## ADD LEXICAL INFORMATION

def addLexicalInformation(logger,reader,query,senses,renaming,lang):

    pos_map  = _read_pos_map()

    for sense in senses:
        sem_args = []
        if sense.has_key('isA'):        sem_args.append(sense['isA'])
        if sense.has_key('subjOfProp'): sem_args.append(sense['subjOfProp'])
        if sense.has_key('objOfProp'):  sem_args.append(sense['objOfProp'])
        sem_args = list(flatten(sem_args))

        extended_entries = []

        for e in sense['entries']: 
            extended_e = dict()

            prefix = reader.signature['domain_name']+lang

            ## canonical form and name for entry in GF
            noCanonical = True
            canonicalForms = query.getResults('canonicalForm',str(e))
            if len(canonicalForms) > 0: 
               canon = readLiteral(canonicalForms[0]).strip()
               if canon != '': 
                  noCanonical = False
                  extended_e['canonicalForm'] = canon.strip()
                  extended_e['gf_name']       = prefix+"_"+toGFidentifier(canon)
               if len(canonicalForms) > 1:
                  logger.too_many_canonicalForms(str(e))
            if noCanonical:
               decomposition = decompose(reader.graph,str(e))
               if decomposition: 
                  extended_e['canonicalForm'] = decomposition
                  extended_e['gf_name']       = prefix+"_"+toGFidentifier(decomposition)
               else:
                  extended_e['gf_name'] = prefix+"_"+toGFidentifier(frag_uri(e,reader.namespaces))
                  logger.no_canonicalForm(str(e))
            
            ## part of speech
            pos = None
            for b in query.getBindingsAsDict('pos',str(e),path=sparql_path_ling):
                if len(b.keys()) > 1: 
                   logger.too_many_pos(str(e))
                for k in b.keys(): 
                    if pos_map.has_key(b[k]):
                       pos = pos_map[b[k]]
                       extended_e['pos'] = pos
                       # and part-of-speech-specific information (if available)
                       p = sparql_subpath_dir(pos)
                       if exists(p): 
                          for f in filter(lambda x: not x.endswith('~'),listdir(p)):
                              query.updateDict(extended_e,str(f).replace('.sparql',''),str(e),lambda x: frag_uri(x,reader.namespaces),sparql_subpath_ling(pos))
                          GFify(lang,extended_e)
                    else: logger.pos_not_found(b[k],str(e))
            if pos is None:
               types = query.getResults('type',str(e))
               if types: extended_e['type'] = map(lambda x: frag_uri(str(x),reader.namespaces),types)

            ## syntactic behaviors
            extended_e['synBehaviors'] = []
            bs     = query.getBindingsAsDict('synBehavior',str(e),path=sparql_path_ling)
            new_bs = []
            for b in bs:
                syn_args = []
                # bring into proper format
                if b.has_key('frame'): 
                   b['frame'] = frag_uri(b['frame'],reader.namespaces)
                old_ks = []
                for k in [ k for k in b.keys() if k != 'frame']: # k = subject, directObject, prepositionalAdjunct, etc.
                    args = []
                    bks  = set([b[k]]) if type(b[k]) != set else b[k]                      
                    for arg in bks:
                        # build arg dict
                        a = dict()
                        if k != 'syn': 
                           syn_args.append(frag_uri(str(arg),reader.namespaces)) 
                           # query for argument-specific information                           
                           query.addToDict(a,'optional',str(arg),readLiteral)
                           query.addToDict(a,'marker',  str(arg),readLiteral)
                           if a.has_key('marker'): 
                              # TODO sanity check: if type(a['marker']) == list
                              a['marker_gfname'] = prefix+"_"+toGFidentifier(a['marker'])
                           # treat prepositional adjuncts as optional
                           if k == 'prepositionalAdjunct': a['optional'] = "true"
                           if a.has_key('optional') and a['optional'] == "false": 
                              del a['optional'] # because mustache can only check for the existence of a field but not its value
                        if not a.keys(): a = frag_uri(arg,reader.namespaces)
                        else:  a['name']   = frag_uri(arg,reader.namespaces)
                        args.append(a) 
                    # add all args (and collaps prepositionalObjects and prepositionalAdjuncts)
                    new_k  = k
                    if k == 'prepositionalObject' or k == 'prepositionalAdjunct': 
                       new_k = 'prepositionalArg'
                       old_ks.append(k)
                    # add all args
                    if len(args) == 1: b[new_k] = args[0]
                    else:              b[new_k] = args
                for old_k in old_ks: del b[old_k]

                # renaming args
                if (sense['reference'],e) in renaming.keys(): 
                    # first check whether syntactic arguments match semantic ones
                    # (if not, this synBehavior belongs to another sense)
                    if [ x for x in syn_args if x not in sem_args ]:
                       continue
                    else: rename(renaming[(sense['reference'],e)],b)
              
                new_bs.append(b)

            # sort per syn
            for syn in list(set([ b['syn'] for b in new_bs if b.has_key('syn') ])):
                copies = []
                for part in [ b for b in new_bs if b['syn'] == syn ]:
                    for k,v in part.items():
                        if k != 'syn':
                           # if optional, add one copy with and one without the argument
                           if not copies: 
                              copies.append({k:v})
                              if _isOptional(v): copies.append(dict())
                           else:
                              copies = _addTo(copies,k,v,_isOptional(v),k == 'prepositionalArg')

                extended_e['synBehaviors'] += copies
                          
            extended_entries.append(extended_e)

        sense['entries'] = extended_entries


## AUXILIARY FUNCTIONS 

def _addTo(copies,k,v,isOpt,isPrep):
    updated_copies = []
    for c in copies:
        # if isOpt, add one copy of c as it was and one copy where v is added to c[k]
        # if isPrep, then build all permutations over c[k]
        new_cs = []
        if isOpt: new_cs.append(deepcopy(c))
        if not c.has_key(k): 
           c[k] = v
           new_cs.append(c)
        else:
           if type(c[k]) == list and v not in c[k]: 
              c[k].append(v)
              new_cs.append(c)
              if isPrep:
                 for perm in permutations(c[k]):
                     new_c    = deepcopy(c)
                     new_c[k] = list(perm)
                     new_cs.append(new_c)
           else:
              if c[k] != v: 
                 ck    = c[k]
                 c[k]  = [ck,v]
                 new_cs.append(c)
                 if isPrep:
                    new_c = deepcopy(c)
                    new_c[k] = [v,ck]
                    new_cs.append(new_c)
              else: new_cs.append(c)
        updated_copies.extend(new_cs)
    return updated_copies

def _isOptional(v):
    return type(v) == dict and v.has_key('optional') and v['optional'] == 'true' 

def rename(renaming_e,source):
    if type(source) == dict: 
       for k in source.keys():
           if type(source[k]) == dict:
              rename(renaming_e,source[k])
           else:
              if source[k] in renaming_e.keys():
                 source[k] =  renaming_e[source[k]]
    elif type(source) == list or type(source) == set:
         for el in source: 
             rename(renaming_e,el)


def _read_pos_map():
    pos_map = dict()
    f_pos_map = open('config/pos_map','r')
    for line in f_pos_map.readlines():
        words = line.split()
        for w in words[1:]:
            pos_map[w] = words[0]
    return pos_map

def _uninstantiated(template,context):
    keys = allkeys(context)
    fields = []
    for s in re.findall('\{\{[\w,\^,\#\/]+\}\}',open(template,'r').read()):
        for x in ['{{','}}','#','/','^']:
            s = s.replace(x,'')
        fields.append(s)
    return list(set([ x for x in fields if x not in keys ]))


## FUNCTIONS ON LINEARIZATIONS

def _collect_lins_opers(tmpl,ls,os,prefix):
# assumes: exactly one complete lin or oper per line
    flag = 'none'

    for line in tmpl.split('\n'):
        line = line.strip()
        if line: 
           m = re.match('^((lin)|(oper))\s*$',line)
           if m: flag = m.group(1)
           else:
             if flag == 'lin':
                if not line in ls: ls.append(line)
             if flag == 'oper' and line != '': 
                spl = line.split('=')
                ids = [ o['left'] for o in os ]
                if len(spl) == 2 and not spl[0].strip() in ids: 
                   os.append({ 'left': spl[0].strip(), 'right': spl[1].strip() })
                else:
                   allthesame = [ o for o in os if o['left'] == spl[0].strip() ]
                   rights = list(set([ o['right'] for o in allthesame]))
                   if len(rights) > 1: 
                      logger.multiple_opers(spl[0],rights)
                      os = (os - allthesame).append({ 'left': spl[0], 'right': max(rights,key=len) })                          
