
import re
import rdflib as rdf

from utils.utils     import *
from utils.RDFreader import getNodeList


lemon = rdf.Namespace("http://www.monnet-project.eu/lemon#")


def readLiteral(term):
    if   type(term) == str: 
         return term
    elif type(term) == rdf.Literal:
         return re.search('.*\"(.*)\".*',term.n3()).group(1)


def getCanonicalForm(graph,x):
    for canon in graph.objects(rdf.URIRef(x),lemon.canonicalForm):
        for rep in graph.objects(canon,lemon.writtenRep):
            return rep.strip()
    return None


def decompose(graph,e):
  
    for dec in graph.objects(rdf.URIRef(e),lemon.decomposition):
 
        nodeList = []
        getNodeList(graph,dec,nodeList)

        fallback = ''
        for node in nodeList:
            el = node
            for o in graph.objects(node,lemon.element): 
                el = o
            c = getCanonicalForm(graph,str(el))
            if c is not None:
               fallback += c + ' '

        if fallback: return fallback.strip()

    return None

