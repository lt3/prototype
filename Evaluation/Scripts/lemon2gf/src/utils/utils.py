import re
import sys


# string utilities

def capitalize1(string):
    if '_' in string:
       s = string.split('_')
       return s[0] +'_'+ capitalize1(s[1])
    else:
       return string[:1].upper() + string[1:]

def snakecase(string):
    return re.sub('\B([A-Z])','_\g<1>',string).lower()

def frag_uri(string,namespaces):
    base = string
    yay  = False
    for k in namespaces.keys():
        if string.startswith(k):
           fragged = string.replace(k,'')
           if fragged.startswith('#') or fragged.startswith('/'):
                 base = namespaces[k] +'_'+ fragged[1:]
           else: base = namespaces[k] +'_'+ fragged
           yay = True; break
    if not yay and '#' in string: 
           base = string.split('#')[1]
    if '/' in base: 
           base = re.match('.*/([^/]+)',base).group(1)
    if base[:1].isdigit(): base = 'blank_' + base
    return base

def frag_file(string):
    if '/' in string: string = string[string.rindex('/')+1:]
    return string[:string.rindex('.')].replace('.','_')

def toGFidentifier(string):
    return filter(lambda x: x.isalnum() or x == '_',
                  string.replace(' ','').replace('-','_').encode('ascii','ignore'))

def frag_ident(string,namespaces):
    return toGFidentifier(frag_uri(string,namespaces))


# list and dict utilities

def flatten(ls):
    for l in ls:
        if hasattr(l,'__iter__'):
            for x in flatten(l):
                yield x
        else:   yield l

def allkeys(d):
    keys = []
    if isinstance(d,dict):
       for k,v in d.items():
           keys.append(k)
           keys += allkeys(v)
    elif isinstance(d,list):
         for x in d:
             keys += allkeys(x)
    else: return keys
    return keys

def intersect(a,b):
     return list(set(a) & set(b))

def listupdate(d1,d2):
    for k in intersect(d1.keys(),d2.keys()):
        if type(d1[k]) == set: d1[k].add(d2[k])
        else: d1[k] = set( [d1[k],d2[k]] )

def updateDictionary(d1,d2):
    for k in d2.keys():
        if k in d1.keys() and d1[k] != d2[k]:
           t1 = type(d1[k]) == list or type(d1[k]) == set 
           t2 = type(d2[k]) == list or type(d2[k]) == set
           if t1:
              if t2: d1[k] += d2[k]
              else:  d1[k].append(d2[k])
           else: 
              if t2: d1[k] = d2[k] + [d1[k]]
              else:  d1[k] = [ d1[k], d2[k] ]
        else: d1[k] = d2[k]

def mergeDictionaries(ds):
    new_dict = None
    for d in ds:
        if new_dict is None: 
           new_dict = d
        else: updateDictionary(new_dict,d)
    return new_dict

def addValues(dictionary,key,values,listsOnly=False):
    if len(values) == 1:
       if not listsOnly:  dictionary[key] = values[0]
       else:              dictionary[key] = values
    elif len(values) > 1: dictionary[key] = values


# signature utilities

def referringExpressions(signature):
    return signature['classes']+signature['relations']+signature['individuals']

def addSuperClass(signature,reference,superclass):
    for c in signature['classes']:
        if c['reference'] == reference:
           c['superclasses'].append(superclass)
           break

# tests

def isEntity(s,signature):
    for e in signature['individuals']:
        if e['reference'] == s : return True
    return False

def isURI(string):
    return ':/' in string

