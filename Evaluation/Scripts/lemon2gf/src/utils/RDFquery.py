
import pystache
import json
import rdflib
import traceback

from os.path   import exists
from sys       import exit
from itertools import chain

from RDFreader import getNodeList, isBlankNode


class RDFquery:

   def __init__(self,logger,graph,path):

       self.logger = logger
       self.graph  = graph
       self.path   = lambda x: path + x +'.sparql'


   """ Querying """

   def check(self,x):
        return exists(self.path(x))

   def q(self,x,y,other=dict()):
       if not exists(x): 
          self.logger.file_not_found(x)
       if y is None:
          return open(x,'r').read()
       else:
          if isBlankNode(y): self.logger.querying_blank_node(x)
          other['uri'] = '<'+y+'>'
          return pystache.render(open(x,'r').read(),other)


   def getResults(self,name,uri=None,path=None):
       if path is None: path = self.path
       return self.getAnswers(self.q(path(name),uri))

   def getAnswers(self,question):
       try:
           answers = self.graph.query(question)
           if rdflib.__version__[:1] == '2':
              return answers.result
           else:
              output = []
              for b in answers.bindings:
                  for v in answers.vars:
                      output.append(b[v])
              return  output
       except: 
           self.logger.querying_failed(question,traceback.format_exc())
           return []


   def addToDict(self,dic,name,uri,postprocessing=lambda x: x): 
       results = self.getResults(name,uri)
       if len(results) == 1:
          dic[name] = postprocessing(results[0])
       elif len(results) > 1:
          dic[name] = map(postprocessing,results)


   def getBindings(self,name,uri,path=None,other=dict()):
       if path is None: path = self.path
       j = None
       try:
           j = self.graph.query(self.q(path(name),uri,other)).serialize(format='json')
           return json.loads(j)['results']['bindings']
       except: 
           self.logger.querying_failed(name,traceback.format_exc())
           return []
    
   def getBindingsAsDict(self,name,uri,postprocessing=lambda x: x,path=None,other=dict()):
       if path is None: path = self.path
       # build [ { key:value } ]
       bindings = []
       for b in self.getBindings(name,uri,path,other):
           d = dict()
           for key in b.keys():
               d[key] = postprocessing(b[key]['value'])
           bindings.append(d)
       # aggregate blank node
       new_bindings = []
       for b in bindings:
           if not new_bindings: new_bindings.append(b)
           else:
              if 'blank' in b.keys():
                 for nb in new_bindings:
                     if 'blank' in nb.keys() and nb['blank'] == b['blank']:
                         listupdate(nb,b)
              else: new_bindings.append(b)
       # remove blank nodes and singletons
       for nb in new_bindings: 
           if nb.has_key('blank'): del nb['blank']
           for (k,v) in nb.items():
               if type(v) == set and len(v) == 1: nb[k] = next(iter(v))
       # return result
       return new_bindings

   def updateDict(self,dic,name,uri,postprocessing=lambda x: x,path=None):
       if path is None: path = self.path
       for b in self.getBindingsAsDict(name,uri,postprocessing,path): 
           # dic.update(b)
           for k in b.keys():
               if dic.has_key(k):
                  if type(dic[k]) == list:
                     dic[k].append(b[k])
                  else: 
                     dic[k] = list(set([ dic[k], b[k] ]))
               else: dic[k] = b[k]

