
import rdflib as rdf
from   rdflib import Namespace, RDF, RDFS, BNode, URIRef
import pystache
import re

from utils import *
import config.global_parameters as config


class RDFreader:

  # Namespaces
  OWL   = Namespace("http://www.w3.org/2002/07/owl#")
  XSD   = Namespace("http://www.w3.org/2001/XMLSchema#")
  lemon = Namespace("http://www.monnet-project.eu/lemon#")

  # Relevant URIs
  classtypes            = [ OWL.Class, RDFS.Class ]
  propertytypes_general = [ RDF.Property, OWL.FunctionalProperty ]
  propertytypes_object  = [ OWL.ObjectProperty, OWL.TransitiveProperty, 
                            OWL.SymmetricProperty, OWL.InverseFunctionalProperty ]
  propertytypes_data    = [ OWL.DatatypeProperty ]
  propertytypes         = propertytypes_general + propertytypes_object + propertytypes_data
  alltypes              = [ OWL.Ontology, RDFS.Datatype, OWL.Restriction, OWL.NamedIndividual ] + classtypes + propertytypes


  def __init__(self,logger,graph,signature,namespaces,blankNodeMap=dict(),ignore=[],lite=False):

      self.logger        = logger
      self.graph         = graph
      self.signature     = signature
      self.namespaces    = namespaces
      self.blankNodeMap  = blankNodeMap
      self.ignore        = ignore + [ self.OWL.Thing ]
      self.lite          = lite

      self.owlThing      = frag_uri(self.OWL.Thing,self.namespaces)
      self.commonSuper   = config.commonSuper(lite)

      self.datatypes     = []


  """
  General 
  """

  def getOntologyURI(self,o,returnAsString=True):

      for uri,_,_ in self.graph.triples((None, RDF.type, self.OWL.Ontology)):
          if returnAsString: return str(uri)
          else:              return uri
      # guess from base prefix
      for line in open(o,'r').readlines():
          m = re.match('@prefix (base)?:.*<(.*)>.*',line)
          if m: return m.group(2); break
      return None


  """ 
  Collect 
  """


  """ Datatypes """

  def collectDatatypes(self):

      datatypes = []

      # explicitely mentioned
      for s,_,_ in self.graph.triples((None, RDF.type, RDFS.Datatype)):
          if not s in datatypes: datatypes.append(s)
      # implicitely mentioned
      for t in self.propertytypes_data:
          for s,_,_ in self.graph.triples((None, RDF.type, t)):
              for _,_,o in self.graph.triples((s, RDFS.range, None)):
                  if o not in datatypes: datatypes.append(o)
     
      for d in datatypes: 
          if not str(d).startswith('http://www.w3.org/2001/XMLSchema#'):
             dt = frag_uri(d,self.namespaces)
             self.signature['datatypes'].append(dt) 

      self.datatypes = datatypes


  def isDatatype(self,uri):
      if str(uri).startswith('http://www.w3.org/2001/XMLSchema#'): 
         return True 
      if uri in self.datatypes: 
         return True
      return False


  """ Classes """

  def collectClasses(self):

    classes = []

    # collect explicitely specified classes
    for t in self.classtypes:
        for s,_,_ in self.graph.triples((None, RDF.type, t)):
            if s not in classes: classes.append(s)

    # collect implicitely mentioned classes
    for _,_,o in self.graph.triples((None, RDF.type, None)):
        if not o in self.classtypes and not o in classes: classes.append(o)
    for s,_,o in self.graph.triples((None, RDFS.subClassOf, None)):
        if s not in classes: classes.append(s)
        if o not in classes: classes.append(o)
    for _,_,o in self.graph.triples((None, RDFS.domain, None)):
        if o not in classes: classes.append(o)
    for _,_,o in self.graph.triples((None, RDFS.range, None)):
        if not self.isDatatype(o) and o not in classes: classes.append(o)

    # filter out OWL and RDF(S) classes
    for t in self.alltypes:
        if t in classes: classes.remove(t)

    # construct classes
    for c in filter(lambda x: x not in self.ignore,classes):
        self.constructClass(c)


  def getSuperClasses(self,uri): 

    superclasses = []
    for _,_,o in self.graph.triples((uri, RDFS.subClassOf, None)):
        sup = self.constructClass(o)
        if sup is not None: superclasses.append(sup)
    return superclasses


  """ Properties """
  
  def collectDatatypeProperties(self):

    dataproperties = []

    # explicitely typed
    for t in self.propertytypes_data:
        for s,_,_ in self.graph.triples((None, RDF.type, t)):
            dataproperties.append(s)
    # properties with datatype as range
    for s,_,o in self.graph.triples((None, RDFS.range, None)):
        if self.isDatatype(o):
           dataproperties.append(s)
    # collect subproperties
    for s in dataproperties:
        for sub in self.getSubPropertiesOf(s,[]):
            dataproperties.append(sub)
    # build properties
    dataproperties = filter(lambda x: x not in self.ignore,dataproperties)
    for s in dataproperties:
        self.constructProperty(s,True)

    return dataproperties


  def collectObjectProperties(self,alsoIgnore=[]):

    properties = []

    for t in self.propertytypes_object:
        for s,_,_ in self.graph.triples((None, RDF.type, t)):
            if s not in self.ignore+alsoIgnore and not s in properties:
               properties.append(s)
    for t in self.propertytypes_general:
        for s,_,_ in self.graph.triples((None, RDF.type, t)):
            if s not in self.ignore+alsoIgnore and not s in properties:
               properties.append(s)
 
    for p in properties:
        self.constructProperty(p,False)
        # collect subproperties
        for sub in self.getSubPropertiesOf(p,[]):
            self.constructProperty(sub,False)


  def getSubPropertiesOf(self,uri,subproperties):
    
    new_properties = []
    for s,_,_ in self.graph.triples((None, RDFS.subPropertyOf, uri)):
        if not s in new_properties: new_properties.append(s)
    subproperties += new_properties
    for n in new_properties:
        for nn in self.getSubPropertiesOf(n,subproperties):
            if not nn in subproperties: subproperties.append(nn)
    return subproperties


  """ Individuals """

  def collectIndividuals(self):

    individuals = []
    for s,_,o in self.graph.triples((None, RDF.type, None)):
        if not o in self.alltypes and not s in individuals:
           individuals.append(s)
           # build dict 
           i_dict = dict()
           i_dict['uri']       = str(s)
           i_dict['reference'] = frag_ident(s,self.namespaces)
           i_dict['type']      = frag_ident(o,self.namespaces)
           # add to signature
           if s not in self.ignore and not self.inSignature('individuals',i_dict):
              self.signature['individuals'].append(i_dict)


  """ 
   Construct 
  """


  def constructSense(self,ref):

    if isURI(ref): uri = URIRef(ref)
    else:          uri = BNode(ref)

    s = self.getFromSignature(uri)
    if s is not None: 
       return s
    if self.isProperty(uri):
       return self.constructProperty(uri)
    if self.isClass(uri):
       return self.constructClass(uri)

    self.logger.sense_construction_failed(str(uri))


  """ Classes """

  def constructClass(self,uri):

    ## check whether it's in ignore or already in signature
    if uri in self.ignore: return None
    c = self.getFromSignature(uri)
    if c is not None: return c 

    c_dict = None 
    ## unionOf
    for _,_,o in self.graph.triples((uri, self.OWL.unionOf, None)):
        c_dict = self.__classList(o,'union')
    ## intersectionOf    
    for _,_,o in self.graph.triples((uri, self.OWL.intersectionOf, None)):
        c_dict = self.__classList(o,'intersection')
    ## complementOf
    for _,_,o in self.graph.triples((uri, self.OWL.complementOf, None)):
        # get noncomplement
        noncomplement = self.constructClass(o)
        # construct dict for complement
        c_dict = dict()
        c_dict['reference']    = 'not_' + noncomplement['reference']
        c_dict['superclasses'] = [ self.commonSuper ]
    ## property restriction
    if self.graph.triples((uri, RDF.type, self.OWL.Restriction)):
        onProperty     = None
        hasValue       = None
        allValuesFrom  = None 
        someValuesFrom = None
        cardinality    = None
        minCardinality = None
        maxCardinality = None 
        for _,_,o in self.graph.triples((uri, self.OWL.onProperty, None)):
            onProperty = self.constructProperty(o)
        for _,_,o in self.graph.triples((uri, self.OWL.hasValue, None)):
            hasValue = toGFidentifier(frag_uri(o,self.namespaces))
        for _,_,o in self.graph.triples((uri, self.OWL.allValuesFrom, None)):
            allValuesFrom  = self.constructClass(o)['reference']
        for _,_,o in self.graph.triples((uri, self.OWL.someValuesFrom, None)):
            someValuesFrom = self.constructClass(o)['reference']
        for _,_,o in self.graph.triples((uri, self.OWL.cardinality, None)):
            cardinality = o
        for _,_,o in self.graph.triples((uri, self.OWL.minCardinality, None)):
            minCardinality = o
        for _,_,o in self.graph.triples((uri, self.OWL.maxCardinality, None)):
            maxCardinality = o
        if onProperty is not None:
            ref = 'Things_with_' + onProperty['reference'] + '_'
            if    hasValue is not None:
                  ref += hasValue
            elif  allValuesFrom is not None:
                  ref += 'all_' + allValuesFrom
            elif  someValuesFrom is not None:
                  ref += 'some_' + someValuesFrom
            elif  cardinality is not None:
                  ref += 'exactly_' + cardinality
            elif  minCardinality is not None:
                  ref += 'min_' + minCardinality
            elif  maxCardinality is not None:
                  ref += 'max_' + maxCardinality
            else: ref += 'whatever'
            c_dict = dict()
            c_dict['reference']    = ref
            c_dict['superclasses'] = [ onProperty['arguments'][0]['name'] ]
    ## add to signature and return
    if c_dict is not None: 
       c_dict['uri'] = str(uri)
       if isBlankNode(uri): self.blankNodeMap[str(uri)] = c_dict['reference']
       if not self.inSignature('classes',c_dict):
          self.signature['classes'].append(c_dict)
    else:
       if isURI(uri):
       ## construct dict and add it to signature
          c_dict = dict()
          c_dict['uri']          = str(uri)
          c_dict['reference']    = frag_ident(uri,self.namespaces)
          c_dict['superclasses'] = []
          for sup in self.getSuperClasses(uri):
              c_dict['superclasses'].append(sup['reference'])
          if not self.commonSuper in c_dict['superclasses']: 
              c_dict['superclasses'].append(self.commonSuper)
          if not self.inSignature('classes',c_dict):
              self.signature['classes'].append(c_dict)
    return c_dict


  def __classList(self,node,kind):

    nodeList = []
    getNodeList(self.graph,node,nodeList)

    if   kind == 'union':        separator = '_or_'
    elif kind == 'intersection': separator = '_and_'
    else:                        separator = '_'

    c_dict = dict()
    c_dict['superclasses'] = [ self.commonSuper ]

    classes = map(lambda x: self.constructClass(x),nodeList)
    # build new reference and add superclass statements
    ref = ''
    for c in classes:
        if c is not None: ref += c['reference'] + separator
        if kind == 'intersection': 
           c_dict['superclasses'].append(c['reference'])
    ref = ref[:-len(separator)]
    if kind == 'union':
       for c in classes: 
           addSuperClass(self.signature,c['reference'],ref)
           x = self.constructClass(ref)
    
    c_dict['reference'] = ref
    return c_dict


  """ Properties """

  def constructProperty(self,uri,isData=False):

    ## check whether it's in signature already
    p = self.getFromSignature(uri)
    if p is not None: return p  

    p_dict = None
    ## inverseOf
    for _,_,o in self.graph.triples((uri, self.OWL.inverseOf, None)):
        # get noninverse
        noninverse = self.constructProperty(o,False)
        # construct dict for inverse
        p_dict = dict()
        p_dict['uri'] = str(uri)
        p_dict['reference'] = noninverse['reference'] + '_1'
        if noninverse.has_key('arguments'):
           p_dict['arguments'] = noninverse['arguments'][::-1]
        else:
           p_dict['arguments'] = []
           p_dict['arguments'].append(self.mkArgument(self.getFromProperty('range',o)))
           p_dict['arguments'].append(self.mkArgument(self.getFromProperty('domain',o)))
    ## propertyChain(Axiom)
    for _,_,o in self.graph.triples((uri, self.OWL.propertyChain, None)):
        p_dict = self.__propertyList(o)
    for _,_,o in self.graph.triples((uri, self.OWL.propertyChainAxiom, None)):
        p_dict = self.__propertyList(o)
    ## add dict to signature and return
    if p_dict is not None:
       if isBlankNode(uri): self.blankNodeMap[str(uri)] = p_dict['reference']
       if not self.inSignature('relations',p_dict):
          self.signature['relations'].append(p_dict)
    else:
       if isURI(uri):
       ## construct dict and add it to signature
          p_dict = dict()
          p_dict['uri']       = str(uri)
          p_dict['reference'] = frag_ident(uri,self.namespaces)
          p_dict['arguments'] = []
          p_dict['arguments'].append(self.mkArgument(self.getFromProperty('domain',uri)))
          p_dict['arguments'].append(self.mkArgument(self.getFromProperty('range',uri),isData))
          if not self.inSignature('relations',p_dict):
             self.signature['relations'].append(p_dict)  
    return p_dict


  def getFromProperty(self,string,uri):

    if   string == 'domain': p = RDFS.domain
    elif string == 'range':  p = RDFS.range
    else: return []

    result = []
    for _,_,o in self.graph.triples((uri, p, None)):
        if self.isDatatype(o):
           result.append(frag_uri(o,self.namespaces))
        else:
           c = self.constructClass(o)
           if c is not None: result.append(c['reference'])
    if not result: return self.commonSuper
    else: 
       if len(result) > 1: self.logger.not_unique(uri,string,str(result),result[0])
       return result[0]


  def __propertyList(self,node):

    nodeList = []
    getNodeList(self.graph,node,nodeList)

    props = map(lambda x: self.constructProperty(x),nodeList)

    p_dict = dict()

    ref = ''
    for p in props: 
        ref += p['reference'] + '_o_'
    p_dict['reference'] = ref[:-3]

    p_dict['arguments'] = []
    p_dict['arguments'].append(props[0]['arguments'][0])    # domain of first prop in list
    p_dict['arguments'].append(props[-1]['arguments'][-1])  # range of last prop in list

    return p_dict


  
  """ 
  Collect
  """

  def collect(self,subj,prop,couldBeList=False):

      uris = []
      for _,_,uri in self.graph.triples((subj, prop, None)):
          if couldBeList: 
             nodes = []
             getNodeList(self.graph,uri,nodes)
             if nodes:
                for node in nodes:
                   uris.append(node)
             else: uris.append(uri)
          else:    uris.append(uri)
      return uris

  """ 
  Add to signature 
  """

  def addtoSignature(self,key,value):
      added = False
      for v in self.signature[key]:
          if v['reference'] == value['reference']:
             if v != value:
                value['reference'] = value['reference'+'_']
                self.signature[key].append(value)
             added = True
             break
      if not added: self.signature[key].append(value)


  """ 
  Auxiliary functions
  """

  def guessURI(self,string):

    guesses = []    
    for s,p,o in self.graph.triples((None, None, None)):
        if string.lower() in str(s).lower(): guesses.append(s)
        if string.lower() in str(p).lower(): guesses.append(p)
        if string.lower() in str(o).lower(): guesses.append(o)
    for guess in guesses:
        g = frag_uri(guess,self.namespaces).lower().split('_')
        if len(g) == 1 and g[0] == string.lower(): 
           return guess
        if len(g) > 1 and '_'.join(g[1:]) == string.lower(): 
           return guess
    return

  def inSignature(self,key,dic):
      for entry in self.signature[key]:
          if entry['reference'] == dic['reference']: 
             return True
      return False

  def getFromSignature(self,uri):
    if self.blankNodeMap.has_key(uri):
       ref = self.blankNodeMap[uri]
    else:
       ref = frag_uri(uri,self.namespaces)
    for e in referringExpressions(self.signature):
        if e['reference'] == ref:
           return e

  def getType(self,uri):
    
    for _,_,o in self.graph.triples((uri, RDF.type, None)):
          return o
    for _,_,o in self.graph.triples((uri, RDFS.subClassOf, None)):
          return self.getType(o)
    for _,_,o in self.graph.triples((uri, RDFS.subPropertyOf, None)):
          return self.getType(o)

  def isProperty(self,uri):

    # check signature
    for p in self.signature['relations']: 
          if p['reference'] == frag_ident(uri,self.namespaces): return True
   # check type
    if self.getType(uri) in self.propertytypes: return True
    # check all other evidence
    for prop in [ self.OWL.inverseOf, self.OWL.propertyChain, self.OWL.propertyChainAxiom ]:
        for _,_,o in self.graph.triples((uri, prop, None)):
            return True
    # give up
    return False

  def isClass(self,uri):
    # check signature
    for c in self.signature['classes']: 
          if c['reference'] == frag_ident(uri,self.namespaces): return True
    # check type
    if self.getType(uri) in self.classtypes+[self.OWL.Restriction]: return True
    # check all other evidence
    for prop in [ self.OWL.complementOf, self.OWL.unionOf, self.OWL.intersectionOf ]:
        for _,_,o in self.graph.triples((uri, prop, None)):
            return True
    # give up
    return False

  def mkArgument(self,string,isData=False): 
      if isData and string != self.commonSuper:
         return dict(name=string,datatype=True)
      else:
         return dict(name=string)


""" 
General-purpose functions 
"""

def isBlankNode(node):
    if type(node) == BNode  or str(node).isalnum(): return True
    return False

def isURI(node):
    if type(node) == URIRef or not str(node).isalnum(): return True
    return False

def getNodeList(graph,node,nodeList):
     for _,_,first in graph.triples((node, RDF.first, None)):
         nodeList.append(first)
     for _,_,rest in graph.triples((node, RDF.rest, None)):
         if not rest == RDF.nil:
            if type(rest) == BNode: 
               getNodeList(graph,rest,nodeList)
            else:
               nodeList.append(rest)
