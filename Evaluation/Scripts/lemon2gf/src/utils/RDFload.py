
import logging
import rdflib as rdf
from sys import exit


"""
 Loading an OWL/RDF graph
"""

def loadGraph(graph,f):

     if f.endswith('.owl') or f.endswith('.rdf'):
           graph.parse(f)
     elif f.endswith('.ttl') or f[:-3] in ('.n3','.nt'):
           graph.parse(f,format='n3') 
     else: 
           logging.error('Unknown format of: '+f)
           exit(1)
     logging.info('Graph in '+ f +' contains ' + str(len(graph)) + ' statements.')

     # for s,p,o in graph: print ' * '+ s +' '+ p +' '+ o +' . '

     return graph

