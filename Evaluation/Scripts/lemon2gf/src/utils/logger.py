
import logging
from sys import exit


class Logger:

  def __init__(self,outputFormat):

      self.outputFormat = outputFormat

      self.abstract  = None
      self.concretes = []

      self.logger = logging.getLogger('')
      self.logger.setLevel(logging.INFO)

      self.logfile = logging.FileHandler('../test/target/lemon2gf.log','w')
      self.console = logging.StreamHandler()
      self.console.setLevel(logging.WARNING)

      self.formatter = logging.Formatter('%(levelname)-8s %(message)s')
      self.logfile.setFormatter(self.formatter)
      self.console.setFormatter(self.formatter)

      self.logger.addHandler(self.logfile)
      self.logger.addHandler(self.console)



  def err(self,msg):
      logging.error(msg)

  def crash(self,msg):
      logging.error(msg)
      exit(1)

  def warn(self,msg):
      logging.warning(msg)

  def info(self,msg):
      logging.info(msg)

  def setAbstract(self,loc):
      self.abstract = loc
  def setConcrete(self,loc):
      self.concretes.append(loc)


  """ Printing """ 

  def printOutput(self,console=True):
      if self.abstract:
         self.info('Abstract syntax written to: ' + self.abstract)
         if console: print 'Abstract syntax written to: ' + self.abstract
      else: 
         self.info('No abstract syntax was constructed.')
         if console: print 'No abstract syntax was constructed.'
      if self.concretes:
         self.info('Concrete syntax(es) written to: ' + ', '.join(self.concretes))
         if console: print 'Concrete syntax(es) written to: ' + ', '.join(self.concretes)
      else: 
         self.info('No concrete syntax was constructed.')
         if console: print 'No concrete syntax was constructed.'
      if console: print 'Log file written to: ../test/target/lemon2gf.log'

  def printSignature(self,signature):
       sig  = '\n------- SIGNATURE -------------------\n\n'
       sig += '\nDomain: ' + signature['domain_name'] + '\n'
       sig += '\nDatatypes:\n'
       for d in signature['datatypes']: sig += str(d) + '\n'
       sig += '\nClasses:\n'
       for c in signature['classes']:      sig += str(c) + '\n'
       sig += '\nRelations:\n '
       for f in signature['relations']:    sig += str(f) + '\n'
       sig += '\nIndividuals:\n '
       for f in signature['individuals']:  sig += str(f) + '\n'
       sig += '\nDefinitions:\n '
       for d in signature['definitions']:  sig += str(d) + '\n'
       sig += '---------------------------------------\n'
       self.info(sig)

  def printRecords(self,records):
       rec = '\n----------------------------\n'
       for r in records: 
           rec += '\n ---- ' 
           if r.has_key('prefix'): rec += r['prefix']
           rec += r['reference'] + ' ---- \n'
           if r.has_key('isA'):        rec += 'isA:        ' + str(r['isA'])        +'\n'
           if r.has_key('subjOfProp'): rec += 'subjOfProp: ' + str(r['subjOfProp']) +'\n'
           if r.has_key('objOfProp'):  rec += 'objOfProp:  ' + str(r['objOfProp'])  +'\n'
           for e in r['entries']:
               rec += ' * '+ str(e) +'\n'
       rec += '----------------------------\n'
       self.info(rec)


  """ Errors """

  def wrong_number_of_languages(self,x): 
      self.crash("Lexicon "+ x +" specifies either no language or more than one.")

  def file_not_found(self,x):
      self.crash("File not found: "+x)

  def querying_failed(self,x,trace):
      self.err("Querying failed: "+ x +"\n"+ trace)

  def querying_blank_node(self,x):
      self.crash("Cannot use a blank node in SPARQL query. (Happened in "+ x +")")

  def missing_reference(self,x):
      self.err("The following sense does not have a reference (and thus is skipped):\n"+ x)

  def sense_construction_failed(self,x):
      self.err('Could not construct sense for '+ x +' (i.e. it was neither in signature nor recognized as class or property).')

  def renaming_overlaps(self,x,y):
      self.err('Renaming overlaps for '+ x + ' (in keys: '+ y +'). This might cause trouble.')

  """ Warnings """

  def no_resource_grammar(self,x): 
      self.warn("There is no resource grammar for language "+ x +". Resulting GF grammar will not compile.")

  def not_unique(self,x,y,z,u):
      self.warn(x+' has more than one '+y+': '+z+'. Picking '+u+'.')

  def no_canonicalForm(self,x): 
      self.warn("No canonicalForm specified for: "+ x +" This might cause problems.")

  def too_many_pos(self,e): 
      self.warn("The following lexical entry has more than one part of speech: "+ e)

  def too_many_canonicalForms(self,e): 
      self.warn("The following lexical entry has more than one canonical form: "+ e)

  def pos_not_found(self,x,e): 
      self.warn("No known part of speech(es) for "+ e +" among: "+ x)

  def no_pos_or_frame(self,x): 
      self.warn("The following entry contains a verbalization without POS or frame (will be skipped): "+ x)

  def no_template(self,x):
      self.warn("No template found for "+ x +".")

  def uninstantiated_fields(self,x,y,e): 
      self.warn("The template "+ x +" for "+ e +" might not be fully instantiated. The following fields are missing in the provided context: "+ str(y))

  def multiple_opers(self,x,y):
      self.warn("There are multiple opers for "+ x +": "+ str(y) +" (Will choose the longest one in the hope that this contains the most information.)")

  def oops(self,x,y): 
      self.warn("Oops, something went wrong when trying to render the following entry:\n" + x + "\n Here is the stack trace:\n" + y)