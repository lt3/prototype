
import sys
import argparse
import rdflib as rdf

from utils.logger       import *
from ontology2abstract  import *
from lexicon2concrete   import *


gf_libs    = dict()
signature  = dict( domain_name = 'Domain',
                   datatypes   = [],
                   classes     = [],
                   relations   = [],
                   individuals = [],
                   lincats     = [],
                   definitions = []
                 )

namespaces = { 'http://www.w3.org/1999/02/22-rdf-syntax-ns#' : 'rdf',
               'http://www.w3.org/2000/01/rdf-schema#'       : 'rdfs',
               'http://www.w3.org/2002/07/owl#'              : 'owl',
               'http://www.w3.org/2001/XMLSchema#'           : 'xsd' }


def run():

    # command line arguments

    p = argparse.ArgumentParser(description='lemon2gf')
    p.add_argument('-o','--ontology', action='append', dest='ontology', 
                   help='path to the ontology (an OWL/RDF file)')
    p.add_argument('-l','--lexicon', action='append', dest='lexica', default=[],
                    help='path to the lexicon (a lemon RDF file)')
    p.add_argument('-m','--module', action='append', dest='modules', default=[],
                    help='load a module (currently supported: conditional, oils)')
    p.add_argument('-p','--prefix', action='store', dest='prefix', default='onto', 
                    help='custom ontology prefix (default: onto)')
    p.add_argument('--lite', action='store_true', dest='lite', 
                    help='generates grammar without sortal restrictions (no dependent types)')
    p.add_argument('--sparql', action='store_true', dest='sparql',
                    help='generates a SPARQL concrete syntax')

    args = p.parse_args()

    # logging

    logger = Logger('txt')
    logger.info('rdflib version: ' + rdf.__version__)

    # init and run

    __init__()

    _generateGF(logger,signature,args.ontology,args.lexica,args.modules,args.prefix,args.lite,args.sparql)



def __init__():

    # language mappings
    for line in open('config/iso639','r').readlines():
        ls = line.split(':')
        if len(ls) == 5: 
           for l in ls[0:4]: gf_libs[l.strip()] = ls[4].strip()


def _generateGF(logger,signature,ontology,lexica,modules,prefix,lite,sparql):

    graph = rdf.Graph()
    graph.open('store',create=True)

    reader = RDFreader(logger,graph,signature,namespaces)

    if ontology: convert_ontology(logger,reader,ontology,modules,prefix,lite,sparql)
    else: logger.warn('No ontology specified.')

    lexicalized_senses = None
    if lexica: lexicalized_senses = convert_lexica(reader,lexica,gf_libs,modules,prefix,lite=lite)
    else: logger.warn('No lexicon specified.')
    
    logger.printSignature(signature)

    render_signature(logger,signature,lite,lexicalized_senses)

    logger.printOutput()



## MAIN #########

if __name__ == "__main__":
    run()

