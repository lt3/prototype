
from pkgutil import iter_modules
from modules import *

def readModules(reader,modules,lite):
    """ collect specific constructs from modules """
    for importer,m,_ in iter_modules(["modules"]):
        if m in modules:
           module = importer.find_module(m).load_module('%s.%s' % ("modules",m))
           module.collect(reader,lite)
