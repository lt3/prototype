
# Linguistic ontology
# requires corresponding folder in src/sparql/
linguistic_ontology = 'lexinfo'

# Default linearization category for Individuals
defaultLincat = 'NP'

# Most general superclass 
def commonSuper(lite):
    if lite: return 'Individual'
    else:    return 'owl_Thing'
