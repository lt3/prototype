

def GFify(lang,source):
    
    if type(source) == dict:
       for k in source.keys():
              if type(source[k]) in [dict,list,set]:
                 GFify(lang,source[k])
              else:
                 source[k] = _genderMap(lang,k,source[k])
                 # and analogously for all other maps to come...
    elif type(source) in [list,set]:
         for el in source: GFify(lang,el)

def _genderMap(lang,key,value):
    return { 'Dut': { 'gender': { 'neuter': 'het',
                                  'commonGender': 'de'
                                }.get(value,value)
                     }.get(key,value)
            }.get(lang,value) 
