
import pystache
import codecs

from utils.RDFload   import *
from utils.utils     import *
from utils.RDFreader import RDFreader
from moduleHandler   import readModules


def convert_ontology(logger,reader,ontology,modules,prefix,lite,sparql):

    # read all ontology files into one graph
    for o in ontology:

        if reader.signature['domain_name'] == 'Domain':
           reader.signature['domain_name'] = toGFidentifier(capitalize1(frag_file(o)))

        reader.graph = loadGraph(reader.graph,o)
        readModules(reader,modules,lite)

        # add ontology URI to namespaces 
        onto = reader.getOntologyURI(o)
        if onto is not None:
           reader.namespaces[onto] = prefix

    # collect ontology constructs 
    reader.collectDatatypes()
    reader.collectClasses()

    dataproperties = reader.collectDatatypeProperties()
    reader.collectObjectProperties(dataproperties)

    reader.collectIndividuals()

    # update namespaces and signature
    namespaces = reader.namespaces
    signature  = reader.signature

    # if --sparql, render SPARQL concrete syntax based on signature
    if sparql:
       source_file = 'templates/custom/SPARQL/concrete.mustache'
       target_file = '../test/target/'+signature['domain_name']+'SPARQL.gf'

       concrete = codecs.open(target_file,'w','utf-8')
       concrete.write(pystache.render(open(source_file,'r').read(),signature))
       concrete.close()
       logger.setConcrete(target_file)


def render_signature(logger,signature,lite,lexicalized_senses):

    if lite:
       source_file = 'templates/abstract.lite.mustache'
    else:
       source_file = 'templates/abstract.full.mustache'
    target_file = '../test/target/'+signature['domain_name']+'.gf'

    abstract_target = codecs.open(target_file,'w','utf-8')
    abstract_target.write(pystache.render(open(source_file,'r').read(),signature))
    abstract_target.close()

    logger.setAbstract(target_file)

