#!/usr/bin/env python2

import sys

try:
  __import__(sys.argv[1])
  #print "Sucessfully import", sys.argv[1]
except:
  print sys.argv[1], "is not installed."
  print "Please install it before using lemon2gf"
  sys.exit(4)

sys.exit(0)
