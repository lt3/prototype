show_menu(){
    NORMAL=`echo "\033[m"`
    MENU=`echo "\033[36m"` #Blue
    NUMBER=`echo "\033[33m"` #yellow
    FGRED=`echo "\033[41m"`
    RED_TEXT=`echo "\033[31m"`
    ENTER_LINE=`echo "\033[33m"`
    echo -e "${MENU}*********************************************${NORMAL}"
    echo -e "${MENU}**${NUMBER} 1)${MENU} Generate sentences from paper ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 2)${MENU} Play around with grammars both domains ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 3)${MENU} Play around with grammar Business Process domain ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 4)${MENU} Play around with grammar Travel domain ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 5)${MENU} Generate the domain grammars using lemon2gf ${NORMAL}"
    echo -e "${MENU}*********************************************${NORMAL}"
    echo -e "${ENTER_LINE}Please enter a menu option and enter or ${RED_TEXT}enter to exit. ${NORMAL}"
    read opt
}

check_install_gf() {
	if ! builtin type -p gf &>/dev/null; then
  		echo "I am sorry, in order to continue GF must be installed"
		echo "Please install GF from:" 
		echo "http://www.grammaticalframework.org/download/index.html"	
		exit;
	fi
}

function option_picked() {
    COLOR='\033[01;31m' # bold red
    RESET='\033[00;00m' # normal white
    MESSAGE=${@:-"${RESET}Error: No message passed"}
    echo -e "${COLOR}${MESSAGE}${RESET}"
}

clear
set -e
show_menu
while [ opt != '' ]
    do
    if [[ $opt = "" ]]; then 
            exit;
    else
	check_install_gf	
        case $opt in
        1) clear;
        	# generate sentences from papers
        ;;

        2) clear;
            cd ../Data/grammars/tools
		    python build_grammar.py -d BusinessProcessDomain -d TravelDomain -t CustomerService -l Eng -l Dut --lite  #&>/dev/null
            rm ../../../Build/*.gf
     		mv ../target/App/*.gf ../../../Build/

		    cd ../../../Scripts/
        	cd ../Build;
		      gf AppEng.gf AppDut.gf;
		    exit;
        ;;

        3) clear;
         	cd ../Data/grammars/tools
             python build_grammar.py -d BusinessProcessDomain -t CustomerService -l Eng -l Dut --lite
            rm ../../../Build/*.gf
 		    mv ../target/App/*.gf ../../../Build/
            cd ../../../Build/
            gf AppEng.gf AppDut.gf;
		    exit;
        ;;

        4) clear;
        	cd ../Data/grammars/tools
             python build_grammar.py -d TravelDomain -t CustomerService -l Eng -l Dut
            rm ../../../Build/*.gf
 		    mv ../target/App/*.gf ../../../Build/
            cd ../../../Build/
            gf AppEng.gf AppDut.gf;
		    exit;
        ;;

	    5) clear;
		    echo "Building grammars for Business Process Domain and Travel Domain using lemon2gf"
            python testimport.py rdflib
            STATUS=$?
            if [ $STATUS = 4 ];
            then
                exit;
            fi
            python testimport.py pystache
            STATUS=$?
            if [ $STATUS = 4 ] ;
            then
                exit;
            fi
		    cd lemon2gf/src
		    python lemon2gf.py -o ../../../Data/Ontologies/travelDomain.owl -o ../../../Data/Ontologies/travelDomain_ABox.ttl -l ../../../Data/Lexica/travel_en.rdf -l ../../../Data/Lexica/travelDomain_nl.ttl -l ../../../Data/Lexica/travelDomain_ABox_en.ttl -l ../../../Data/Lexica/travelDomain_ABox_nl.ttl -p travel --lite #&>/dev/null
		    mv ../test/target/TravelDomain* ../../../Data/grammars/gf/domain/
            echo "Done building Travel Domain grammar"
            echo 
            python lemon2gf.py -o ../../../Data/Ontologies/BusinessProcessDomain.owl -l ../../../Data/Lexica/BusinessProcessDomainLexicon_en.rdf -l ../../../Data/Lexica/BusinessProcessDomainLexicon_nl.rdf -p BI --lite #&>/dev/null
     		mv ../test/target/BusinessProcessDomain* ../../../Data/grammars/gf/domain/
            echo "Done building Business Process Domain grammar"
            echo 
		    cd ../../../Data/grammars/tools
		    python build_grammar.py -d BusinessProcessDomain -d TravelDomain -t CustomerService -l Eng -l Dut --lite  #&>/dev/null
            rm ../../../Build/*.gf
     		mv ../target/App/*.gf ../../../Build/

		    cd ../../../Scripts/
		    echo "Grammars stored in ../Build"
		    show_menu;
            ;;


        x)exit;
        ;;

        \n)exit;
        ;;

        *)clear;
        option_picked "Pick an option from the menu";
        show_menu;
        ;;
    esac
fi
done

